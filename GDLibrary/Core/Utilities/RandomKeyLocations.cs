﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.Linq;

namespace GDLibrary.Core.Utilities
{
    public class RandomKeyLocations
    {
        #region Fields
        private Random random = new Random();

        private Vector3 diamondKeyLocation;
        private Vector3 heartKeyLocation;
        private Vector3 spadeKeyLocation;

        private List<Vector3> bedroomKeyLocs;
        private List<Vector3> diningKeyLocs;
        private List<Vector3> kitchenKeyLocs;
        private List<Vector3> livingKeyLocs;

        private List<List<Vector3>> listOfRooms;
        private List<Vector3> keyLocations;

        private Dictionary<string, List<Vector3>> roomKeyLocs;
        #endregion

        #region Properties
        public Vector3 DiamondKeyLocation { get => diamondKeyLocation; }
        public Vector3 HeartKeyLocation { get => heartKeyLocation; }
        public Vector3 SpadeKeyLocation { get => spadeKeyLocation; }
        public List<Vector3> KeyLocations { get => keyLocations; }
        #endregion

        public RandomKeyLocations()
        {
            // Vector3 key locations
            this.diamondKeyLocation = new Vector3(0, 0, 0);
            this.heartKeyLocation = new Vector3(0, 0, 0);
            this.spadeKeyLocation = new Vector3(0, 0, 0);

            // Lists of rooms that have multiple key locations
            this.bedroomKeyLocs = new List<Vector3>();
            this.diningKeyLocs = new List<Vector3>();
            this.kitchenKeyLocs = new List<Vector3>();
            this.livingKeyLocs = new List<Vector3>();
            this.keyLocations = new List<Vector3>();

            this.listOfRooms = new List<List<Vector3>>();


            // Dictionary of door keys with the value of a list of key locations
            this.roomKeyLocs = new Dictionary<string, List<Vector3>>();

            // Methods
            populateLists();
            shuffleLists();
            addListsToDict();
            calculateLocations();
        }

        private void populateLists()
        {
            // ADD MORE KEY LOCATIONS WHEN KNOWN ********************************
            // Bedroom
            this.bedroomKeyLocs.Add(new Vector3(70f, 2.8f, -133f)); // under right middle of bed
            this.bedroomKeyLocs.Add(new Vector3(75f, 2.8f, -138.5f)); // between left bedside drawer and bed
            this.bedroomKeyLocs.Add(new Vector3(75f, 2.8f, -129f)); // between right bedside drawer and bed
            this.bedroomKeyLocs.Add(new Vector3(55f, 2.8f, -101f)); // left of cabinet
            this.bedroomKeyLocs.Add(new Vector3(27f, 2.8f, -101f)); // corner right of cabinet
            this.bedroomKeyLocs.Add(new Vector3(50f, 5.9f, -101.6f)); // On cabinet
            this.bedroomKeyLocs.Add(new Vector3(75f, 2.8f, -101f)); // corner left of cabinet
            this.bedroomKeyLocs.Add(new Vector3(65f, 8.4f, -101f)); // On shelf
            this.bedroomKeyLocs.Add(new Vector3(50f, 5.6f, -161.25f)); // top of bedroom dresser table

            // Dining room
            this.diningKeyLocs.Add(new Vector3(-47f, 2.8f, -98f)); // left of shelves
            this.diningKeyLocs.Add(new Vector3(-44f, 8.35f, -98f)); // On left shelf
            this.diningKeyLocs.Add(new Vector3(-38f, 4.35f, -98f)); // On middle shelf
            this.diningKeyLocs.Add(new Vector3(-31f, 9.9f, -98f)); // On right shelf
            this.diningKeyLocs.Add(new Vector3(-28f, 2.8f, -98f)); // corner right of shelves
            this.diningKeyLocs.Add(new Vector3(-28f, 2.8f, -39f)); // corner near table
            this.diningKeyLocs.Add(new Vector3(-73f, 2.8f, -39f)); // corner far from table
            this.diningKeyLocs.Add(new Vector3(-47f, 2.8f, -60f)); // under table

            // Kitchen                                                  
            this.kitchenKeyLocs.Add(new Vector3(-63f, 2.8f, -161f)); // kitchen floor left of sink
            this.kitchenKeyLocs.Add(new Vector3(-59f, 6.5f, -162f)); // kitchen counter left of sink
            this.kitchenKeyLocs.Add(new Vector3(-27f, 2.8f, -103f)); // left of fridge
            this.kitchenKeyLocs.Add(new Vector3(-29.8f, 10.6f, -103f)); // top of fridge left
            this.kitchenKeyLocs.Add(new Vector3(-33f, 10.6f, -103f)); // top of fridge right
            this.kitchenKeyLocs.Add(new Vector3(-36f, 2.8f, -103f)); // right of fridge
            this.kitchenKeyLocs.Add(new Vector3(-42f, 2.8f, -128f)); // under kitchen table
            this.kitchenKeyLocs.Add(new Vector3(-52f, 6.3f, -128f)); // on kitchen table

            // Living room
            this.livingKeyLocs.Add(new Vector3(73f, 2.8f, -97f)); // corner beside doll in rocking chair
            this.livingKeyLocs.Add(new Vector3(71f, 3.9f, -43f)); // under tv bottom shelf
            this.livingKeyLocs.Add(new Vector3(72f, 6f, -43f)); // behind tv
            this.livingKeyLocs.Add(new Vector3(72f, 4.65f, -44f)); // under tv top shelf
            this.livingKeyLocs.Add(new Vector3(27.5f, 2.8f, -79f)); // right of gramophone table
            this.livingKeyLocs.Add(new Vector3(27f, 2.8f, -75f)); // under gramophone table
            this.livingKeyLocs.Add(new Vector3(51.5f, 5.07f, -60f)); // On couch
            this.livingKeyLocs.Add(new Vector3(27f, 5.8f, -76.5f)); // On gramophone table
            this.livingKeyLocs.Add(new Vector3(54.5f, 2.8f, -63f)); // Under couch

            // List of rooms
            this.listOfRooms.Add(bedroomKeyLocs);
            this.listOfRooms.Add(diningKeyLocs);
            this.listOfRooms.Add(kitchenKeyLocs);
            this.listOfRooms.Add(livingKeyLocs);
        }

        private void shuffleLists()
        {
            // Shuffle Lists

            for (int i = 0; i < listOfRooms.Count; i++)
            {
                for (int j = 0; j < listOfRooms[i].Count; j++)
                {
                    int k = random.Next(0, j + 1);
                    Vector3 value = listOfRooms[i][k];
                    listOfRooms[i][k] = listOfRooms[i][j];
                    listOfRooms[i][j] = value;
                }
            }

            this.listOfRooms = listOfRooms.OrderBy(x => random.Next()).ToList();
        }

        private void addListsToDict()
        {
            // Add Lists to Dictionary 
            this.roomKeyLocs.Add("Diamond", listOfRooms.ElementAt(0));
            this.roomKeyLocs.Add("Heart", listOfRooms.ElementAt(1));
            this.roomKeyLocs.Add("Spade", listOfRooms.ElementAt(2));
            this.roomKeyLocs.Add("Spare", listOfRooms.ElementAt(3));
        }

        private void calculateLocations()
        {
            foreach (string key in roomKeyLocs.Keys)
            {
                if (key.Equals("Diamond"))
                    this.diamondKeyLocation = roomKeyLocs[key].ElementAt(0);

                else if (key.Equals("Heart"))
                    this.heartKeyLocation = roomKeyLocs[key].ElementAt(0);

                else if (key.Equals("Spade"))
                    this.spadeKeyLocation = roomKeyLocs[key].ElementAt(0);

                this.keyLocations.Add(this.diamondKeyLocation);
                this.keyLocations.Add(this.heartKeyLocation);
                this.keyLocations.Add(this.spadeKeyLocation);
            }
        }

        public void printAllLocations()
        {
            foreach (List<Vector3> list in this.roomKeyLocs.Values)
            {
                foreach (Vector3 vector3 in list)
                {
                    System.Diagnostics.Debug.WriteLine("Door key Location: " + vector3);
                }
            }
        }

        public void printDoorKeyLocations()
        {
            System.Diagnostics.Debug.WriteLine("Diamond Key: " + diamondKeyLocation);
            System.Diagnostics.Debug.WriteLine("Heart Key: " + heartKeyLocation);
            System.Diagnostics.Debug.WriteLine("Spade Key: " + spadeKeyLocation);
        }
    }
}
