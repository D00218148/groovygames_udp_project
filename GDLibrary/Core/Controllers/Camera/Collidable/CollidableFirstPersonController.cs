﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary.Controllers
{
    
    /// <summary>
    /// First person COLLIDABLE camera controller.
    /// A collidable camera has a body and collision skin from a player object but it has no modeldata or texture
    /// </summary>
    public class CollidableFirstPersonCameraController : UserInputController
    {
        #region Fields
        private PlayerObject playerObject;
        private float radius, height;
        private float accelerationRate, decelerationRate, mass, jumpHeight;
        private Vector3 translationOffset;
        private bool rightAltHeld = false;
        private bool rightCtrlEnabled = false;
        private float rotationX;
        private float rotationY;
        private float attenuator = 400;
        private int escaped = 0;
        private bool trapped = false;

        SoundManager soundManager;

        //private bool isCrouching = false;
        #endregion Fields

        #region Properties
        public PlayerObject PlayerObject { get => playerObject; set => playerObject = value; }
        public float Radius { get => radius; set => radius = value; }
        public float Height { get => height; set => height = value; }
        public float AccelerationRate { get => accelerationRate; set => accelerationRate = value; }
        public float DecelerationRate { get => decelerationRate; set => decelerationRate = value; }
        public float Mass { get => mass; set => mass = value; }
        public float JumpHeight { get => jumpHeight; set => jumpHeight = value; }

        private GameStateManager gameStateManager;

        public Vector3 TranslationOffset { get => translationOffset; set => translationOffset = value; }
        #endregion Properties

        //allows developer to specify the type of collidable object to be used as basis for the camera
        public CollidableFirstPersonCameraController(string id, ControllerType controllerType,
            KeyboardManager keyboardManager,
            MouseManager mouseManager,
            GamePadManager gamePadManager,
            GameStateManager gameStateManager,
            Keys[] moveKeys, float moveSpeed, float strafeSpeed, float rotationSpeed,
            //below this is PlayerObject
            IActor parentActor, Vector3 translationOffset,
            float radius, float height, float accelerationRate, float decelerationRate, float mass, float jumpHeight)
            : base(id, controllerType,
            keyboardManager, mouseManager, gamePadManager,
            moveKeys, moveSpeed, strafeSpeed, rotationSpeed)
        {
            Radius = radius;
            Height = height;

            AccelerationRate = accelerationRate;
            DecelerationRate = decelerationRate;
            Mass = mass;
            JumpHeight = jumpHeight;

            this.gameStateManager = gameStateManager;

            //allows us to tweak the camera position within the player object
            TranslationOffset = translationOffset;

            //to do...instanciate player object
            playerObject = new PlayerObject(ID + " - player object",
                ActorType.CollidableCamera, StatusType.Update, (parentActor as Actor3D).Transform3D,
                null, null, //no model, no texture,
                MoveKeys, this.radius, this.height, this.accelerationRate, this.decelerationRate,
                this.jumpHeight, this.translationOffset, keyboardManager);

            playerObject.Enable(false, this.mass);

            playerObject.Body.CollisionSkin.callbackFn += HandleCollision;
        }

        private bool HandleCollision(CollisionSkin collider, CollisionSkin collidee)
        {
            //step 3 - cast the collidee (the object you hit) to access its fields
            CollidableObject collidableObject = collidee.Owner.ExternalData as CollidableObject;

            //step 4 - make a decision on what you're going to do based on, say, the ActorType
            if (collidableObject.ActorType == ActorType.Win && gameStateManager.KeysCollected == 3)
            {
                EventDispatcher.Publish(new EventData(EventCategoryType.WinLose, EventActionType.OnWin, null));
            }

            return true;
        }

        public override void HandleKeyboardInput(GameTime gameTime, Actor3D parentActor)
        {
            HandleForwardBackward(gameTime, parentActor);
            HandleStrafeLeftRight(gameTime, parentActor);
            HandleJump(gameTime, parentActor);
            HandleCrouch(gameTime, parentActor);

            //move the camera to wherever the player object capsule is
            parentActor.Transform3D.Translation = playerObject.CharacterBody.Position + this.translationOffset;

            //   base.HandleKeyboardInput(gameTime, parentActor);
        }

        public override void HandleMouseInput(GameTime gameTime, Actor3D parentActor)
        {
            //MouseManager.MouseVisible = true;
            HandleMouseRotation(gameTime, parentActor);
            CaughtInTrap(gameTime, parentActor);

            //base.HandleMouseInput(gameTime, parentActor);
        }

        public override void HandleGamePadInput(GameTime gameTime, Actor3D parentActor)
        {

            if (GamePadManager.IsPlayerConnected(PlayerIndex.One))
            {
                HandleGamePadForwardBackward(gameTime, parentActor);
                HandleGamePadStrafeLeftRight(gameTime, parentActor);
                HandleGamePadJump();
                HandleGamePadCrouch();
                HandleGamePadRotation(gameTime, parentActor);
            }

            //move the camera to wherever the player object capsule is
            parentActor.Transform3D.Translation = playerObject.CharacterBody.Position + this.translationOffset;

            //base.HandleGamePadInput(gameTime, parentActor);
        }

        private void HandleGamePadRotation(GameTime gameTime, Actor3D parentActor)
        {
            //MouseManager.MouseVisible = true;
            GamePadThumbSticks thumbSticks = GamePadManager.GetThumbSticks(PlayerIndex.One);

            if (GamePadManager.IsPlayerConnected(PlayerIndex.One))
            {
                float totalRotationSpeed = RotationSpeed * attenuator * gameTime.ElapsedGameTime.Milliseconds;

                if (thumbSticks.Right.X > 0 || thumbSticks.Right.X < 0 || thumbSticks.Right.Y > 0 || thumbSticks.Right.Y < 0)
                {

                    // X movement - right and left
                    if (thumbSticks.Right.X > 0.01)
                    {
                        if (thumbSticks.Right.X <= 0.2)
                        {
                            rotationX += MathHelper.ToRadians(totalRotationSpeed / 20);
                        }
                        else if (thumbSticks.Right.X <= 0.4)
                        {
                            rotationX += MathHelper.ToRadians(totalRotationSpeed / 15);
                        }
                        else if (thumbSticks.Right.X <= 0.6)
                        {
                            rotationX += MathHelper.ToRadians(totalRotationSpeed / 10);
                        }
                        else if (thumbSticks.Right.X <= 0.8)
                        {
                            rotationX += MathHelper.ToRadians(totalRotationSpeed / 5);
                        }
                        else if (thumbSticks.Right.X <= 1)
                        {
                            rotationX += MathHelper.ToRadians(totalRotationSpeed);
                        }
                    }
                    else if (thumbSticks.Right.X < -0.01)
                    {
                        if (thumbSticks.Right.X >= -0.2)
                        {
                            rotationX -= MathHelper.ToRadians(totalRotationSpeed / 20);
                        }
                        else if (thumbSticks.Right.X >= -0.4)
                        {
                            rotationX -= MathHelper.ToRadians(totalRotationSpeed / 15);
                        }
                        else if (thumbSticks.Right.X >= -0.6)
                        {
                            rotationX -= MathHelper.ToRadians(totalRotationSpeed / 10);
                        }
                        else if (thumbSticks.Right.X >= -0.8)
                        {
                            rotationX -= MathHelper.ToRadians(totalRotationSpeed / 5);
                        }
                        else if (thumbSticks.Right.X >= -1)
                        {
                            rotationX -= MathHelper.ToRadians(totalRotationSpeed);
                        }
                    }

                    // Y movement - up and down
                    if (thumbSticks.Right.Y > 0 && parentActor.Transform3D.RotationInDegrees.Y < 90)
                    {
                        rotationY += MathHelper.ToRadians(totalRotationSpeed / 2);                        
                    }
                    else if (thumbSticks.Right.Y < 0 && parentActor.Transform3D.RotationInDegrees.Y > -90)
                    {
                        rotationY -= MathHelper.ToRadians(totalRotationSpeed / 2);                                               
                    }

                    this.MouseManager.SetPosition(new Vector2(this.MouseManager.Position.X, this.MouseManager.Position.Y));

                    parentActor.Transform3D.RotateBy(new Vector3(-rotationX, rotationY, 0));
                }
            }
        }

        private void HandleForwardBackward(GameTime gameTime, Actor3D parentActor)
        {
            System.Diagnostics.Debug.WriteLine("\n\nTranslation: " + parentActor.Transform3D.Translation +"***********************************************************\n\n");

            Vector3 restrictedLook = parentActor.Transform3D.Look;
            restrictedLook.Y = 0;

            if (KeyboardManager.IsKeyDown(MoveKeys[0]) || KeyboardManager.IsKeyDown(MoveKeys[7])) //[W] or [UP]
            {
                if (KeyboardManager.IsKeyDown(MoveKeys[4]) || KeyboardManager.IsKeyDown(MoveKeys[11])) //[Lshift] or [Rshift]
                {
                    if (playerObject.CharacterBody.IsCrouching)
                    {
                        playerObject.CharacterBody.Velocity -= (MoveSpeed * GameConstants.runMultiplier - 0.33f) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                    {
                        // RUN FORWARD
                        playerObject.CharacterBody.Velocity += (MoveSpeed * GameConstants.runMultiplier) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                        //playerObject.CharacterBody.IsCrouching = false;
                    }
                }
                else
                {
                    if (playerObject.CharacterBody.IsCrouching)
                    {
                        playerObject.CharacterBody.Velocity -= (MoveSpeed * GameConstants.runMultiplier - 0.3f) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                    {
                        // WALK FORWARD
                        playerObject.CharacterBody.Velocity += MoveSpeed * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                }
            }
            else if (KeyboardManager.IsKeyDown(MoveKeys[1]) || KeyboardManager.IsKeyDown(MoveKeys[8])) //[S] or [DOWN]
            {
                if (KeyboardManager.IsKeyDown(MoveKeys[4]) || KeyboardManager.IsKeyDown(MoveKeys[11])) //[Lshift] or [Rshift]
                {
                    if (playerObject.CharacterBody.IsCrouching)
                    {
                        playerObject.CharacterBody.Velocity += (MoveSpeed * GameConstants.runMultiplier - 0.33f) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                    {
                        // RUN BACKWARDS (slower than run forwards)
                        playerObject.CharacterBody.Velocity -= (MoveSpeed * (GameConstants.runMultiplier - 0.5f)) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                }
                else
                {
                    if (playerObject.CharacterBody.IsCrouching)
                    {
                        playerObject.CharacterBody.Velocity += (MoveSpeed * GameConstants.runMultiplier - 0.3f) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                    else
                    {
                        // WALK BACKWARDS
                        playerObject.CharacterBody.Velocity -= MoveSpeed * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                    }
                }
            }
            else //when we takes off forward or backward
            {
                playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
            }
        }

        private void HandleGamePadForwardBackward(GameTime gameTime, Actor3D parentActor)
        {
            GamePadThumbSticks thumbSticks = GamePadManager.GetThumbSticks(PlayerIndex.One);
            Vector3 restrictedLook = parentActor.Transform3D.Look;
            restrictedLook.Y = 0;

            if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.DPadUp) || thumbSticks.Left.Y > 0.1f)
            {
                if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightShoulder))
                {
                    // RUN FORWARD
                    playerObject.CharacterBody.Velocity += (MoveSpeed * GameConstants.runMultiplier) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // WALK FORWARD
                    playerObject.CharacterBody.Velocity += MoveSpeed * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                }

            }
            else if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.DPadDown) || thumbSticks.Left.Y < -0.1f)
            {
                if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightShoulder))
                {
                    // RUN BACKWARDS (slower than run forwards)
                    playerObject.CharacterBody.Velocity -= (MoveSpeed * (GameConstants.runMultiplier - 0.5f)) * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // WALK BACKWARDS
                    playerObject.CharacterBody.Velocity -= MoveSpeed * restrictedLook * gameTime.ElapsedGameTime.Milliseconds;
                }
            }
            else //when we takes off forward or backward
            {
                playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
            }
        }

        private void HandleStrafeLeftRight(GameTime gameTime, Actor3D parentActor)
        {
            //strafe left/right
            if (KeyboardManager.IsKeyDown(MoveKeys[2]) || KeyboardManager.IsKeyDown(MoveKeys[9])) //[A] or [LEFT]
            {
                Vector3 restrictedRight = parentActor.Transform3D.Right;
                restrictedRight.Y = 0;

                if (KeyboardManager.IsKeyDown(MoveKeys[4]) || KeyboardManager.IsKeyDown(MoveKeys[11])) //[Lshift] or [Rshift]
                {
                    // STRAFE FASTER LEFT
                    playerObject.CharacterBody.Velocity -= restrictedRight * (StrafeSpeed * (GameConstants.runMultiplier - 0.3f)) * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // STRAFE LEFT
                    playerObject.CharacterBody.Velocity -= restrictedRight * StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }

            }
            else if (KeyboardManager.IsKeyDown(MoveKeys[3]) || KeyboardManager.IsKeyDown(MoveKeys[10])) //[D] or [RIGHT])
            {
                Vector3 restrictedRight = parentActor.Transform3D.Right;
                restrictedRight.Y = 0;

                if (KeyboardManager.IsKeyDown(MoveKeys[4]) || KeyboardManager.IsKeyDown(MoveKeys[11])) //[Lshift] or [Rshift]
                {
                    // STRAFE FASTER RIGHT
                    playerObject.CharacterBody.Velocity += restrictedRight * (StrafeSpeed * (GameConstants.runMultiplier - 0.3f)) * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // STRAFE RIGHT
                    playerObject.CharacterBody.Velocity += restrictedRight * StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }
            }
            else //decelerate to zero when not pressed
            {
                playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
            }
        }

        private void HandleJump(GameTime gameTime, Actor3D parentActor)
        {
            if (KeyboardManager.IsKeyDown(MoveKeys[6])) //check GameConstants.CameraMoveKeys for correct index of each move key
            {
                playerObject.CharacterBody.DoJump(jumpHeight);
            }
        }

        private void HandleCrouch(GameTime gameTime, Actor3D parentActor)
        {
            //playerObject.CharacterBody.IsCrouching = false;

            //Press Right Alt key to toggle which Ctrl key to crouch
            if (KeyboardManager.IsKeyDown(MoveKeys[13]))
                rightAltHeld = true;

            if (KeyboardManager.IsKeyUp(MoveKeys[13]) && rightAltHeld && rightCtrlEnabled == false)
            {
                rightCtrlEnabled = true;
                rightAltHeld = false;
            }
            else if (KeyboardManager.IsKeyUp(MoveKeys[13]) && rightAltHeld && rightCtrlEnabled)
            {
                rightCtrlEnabled = false;
                rightAltHeld = false;
            }


            //crouch
            if (rightCtrlEnabled && rightAltHeld == false)
            {
                //CROUCHING FOR RIGHT HAND ONLY
                if (KeyboardManager.IsKeyDown(MoveKeys[12]) && (playerObject.CharacterBody.IsCrouching == false))
                {
                    TranslationOffset = new Vector3(0, -1f, 0);
                    playerObject.CharacterBody.IsCrouching = true;
                }
                else if (KeyboardManager.IsKeyUp(MoveKeys[12]) && playerObject.CharacterBody.IsCrouching == true)
                {
                    TranslationOffset = new Vector3(0, 2f, 0);
                    playerObject.CharacterBody.IsCrouching = false;
                }
            }
            else if (rightCtrlEnabled == false && rightAltHeld == false)
            {
                // CROUCHING FOR LEFT HAND ONLY
                if (KeyboardManager.IsKeyDown(MoveKeys[5]) && (playerObject.CharacterBody.IsCrouching == false))
                {
                    TranslationOffset = new Vector3(0, -1f, 0);
                    playerObject.CharacterBody.IsCrouching = true;
                }
                else if (KeyboardManager.IsKeyUp(MoveKeys[5]) && playerObject.CharacterBody.IsCrouching == true)
                {
                    TranslationOffset = new Vector3(0, 2f, 0);
                    playerObject.CharacterBody.IsCrouching = false;
                }
            }
        }

        private void HandleMouseRotation(GameTime gameTime, Actor3D parentActor)
        {
            //MouseManager.MouseVisible = false;
            Vector2 mouseDelta = MouseManager.GetDeltaFromCentre(new Vector2(512, 384)); //REFACTOR - NMCG
            mouseDelta *= RotationSpeed * gameTime.ElapsedGameTime.Milliseconds;

            if (mouseDelta.Length() != 0)
            {
                if (this.MouseManager.HasMovedRight(0.01f))
                {
                    if (this.MouseManager.Position.X > 1022.9f)
                    {
                        float mousePosY = this.MouseManager.Position.Y;

                        this.MouseManager.SetPosition(new Vector2(1, mousePosY));
                    }
                }
                else if (this.MouseManager.HasMovedLeft(0.01f))
                {
                    if (this.MouseManager.Position.X < 1)
                    {
                        float mousePosY = this.MouseManager.Position.Y;

                        this.MouseManager.SetPosition(new Vector2(1022.9f, mousePosY));
                    }
                }
                parentActor.Transform3D.RotateBy(new Vector3(-2.2f * mouseDelta.X, -1 * mouseDelta.Y, 0));
            }
        }

        private void HandleGamePadCrouch()
        {
            if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.LeftShoulder))
            {
                TranslationOffset = new Vector3(0, -1f, 0);
                playerObject.CharacterBody.IsCrouching = true;
                System.Diagnostics.Debug.WriteLine("\n\nLeftShoulder button Pressed! ***********************************************************\n\n");
            }
        }

        private void HandleGamePadStrafeLeftRight(GameTime gameTime, Actor3D parentActor)
        {
            GamePadThumbSticks gpts = GamePadManager.GetThumbSticks(PlayerIndex.One);

            //strafe left/right
            if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.DPadLeft) || gpts.Left.X < -0.1f)
            {
                Vector3 restrictedRight = parentActor.Transform3D.Right;
                restrictedRight.Y = 0;

                if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightShoulder)) //[Lshift] or [Rshift]
                {
                    // STRAFE FASTER LEFT
                    playerObject.CharacterBody.Velocity -= restrictedRight * (StrafeSpeed * (GameConstants.runMultiplier - 0.3f)) * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // STRAFE LEFT
                    playerObject.CharacterBody.Velocity -= restrictedRight * StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }

            }
            else if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.DPadRight) || gpts.Left.X > 0.1f)
            {
                Vector3 restrictedRight = parentActor.Transform3D.Right;
                restrictedRight.Y = 0;

                if (GamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightShoulder)) //[Lshift] or [Rshift]
                {
                    // STRAFE FASTER RIGHT
                    playerObject.CharacterBody.Velocity += restrictedRight * (StrafeSpeed * (GameConstants.runMultiplier - 0.3f)) * gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                {
                    // STRAFE RIGHT
                    playerObject.CharacterBody.Velocity += restrictedRight * StrafeSpeed * gameTime.ElapsedGameTime.Milliseconds;
                }
            }
            else //decelerate to zero when not pressed
            {
                playerObject.CharacterBody.DesiredVelocity = Vector3.Zero;
            }
        }

        private void HandleGamePadJump()
        {
            if (GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
            {
                System.Diagnostics.Debug.WriteLine("\n\nA button Pressed! ***********************************************************\n\n");
                playerObject.CharacterBody.DoJump(jumpHeight);
            }
        }

        private void CaughtInTrap(GameTime gameTime, Actor3D parentActor)
        {

            // Dining Room Traps
            if (playerObject.CharacterBody.Transform.Position.X < -43.5 && playerObject.CharacterBody.Transform.Position.X > -46.5
                && playerObject.CharacterBody.Transform.Position.Z < -44.5 && playerObject.CharacterBody.Transform.Position.Z > -47.5
                && playerObject.CharacterBody.Position.Y <= 7) 
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }

                playerObject.CharacterBody.Position = new Vector3(-45, 4, -46);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {
                    
                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(-45, 7, -46);

                        escaped = 0;
                        trapped = false;
                    }
                }
            }

            if (playerObject.CharacterBody.Transform.Position.X < -34.5 && playerObject.CharacterBody.Transform.Position.X > -37.5
                && playerObject.CharacterBody.Transform.Position.Z < -83.5 && playerObject.CharacterBody.Transform.Position.Z > -86.5
                && playerObject.CharacterBody.Position.Y <= 7)
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }

                playerObject.CharacterBody.Position = new Vector3(-36f, 4, -85f);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {

                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(-36f, 7, -85f);

                        escaped = 0;
                        trapped = false;
                    }
                }
            }

            // Kitchen Traps
            if (playerObject.CharacterBody.Transform.Position.X < -56.5 && playerObject.CharacterBody.Transform.Position.X > -59.5
                && playerObject.CharacterBody.Transform.Position.Z < -144.5 && playerObject.CharacterBody.Transform.Position.Z > -147.5
                && playerObject.CharacterBody.Position.Y <= 7)
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }

                playerObject.CharacterBody.Position = new Vector3(-58f, 4, -146f);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {

                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(-58f, 7, -146f);

                        escaped = 0;
                        trapped = false;

                    }
                }
            }

            if (playerObject.CharacterBody.Transform.Position.X < -36.5 && playerObject.CharacterBody.Transform.Position.X > -39.5
                && playerObject.CharacterBody.Transform.Position.Z < -118.5 && playerObject.CharacterBody.Transform.Position.Z > -121.5
                && playerObject.CharacterBody.Position.Y <= 7)
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }

                playerObject.CharacterBody.Position = new Vector3(-38f, 4, -120f);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {

                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(-38f, 7, -120f);

                        escaped = 0;
                        trapped = false;

                    }
                }
            }

            // bedroom trap
            if (playerObject.CharacterBody.Transform.Position.X < 51.5 && playerObject.CharacterBody.Transform.Position.X > 48.5
                && playerObject.CharacterBody.Transform.Position.Z < -148.5 && playerObject.CharacterBody.Transform.Position.Z > -151.5
                && playerObject.CharacterBody.Position.Y <= 7)
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }
                
                playerObject.CharacterBody.Position = new Vector3(50f, 4, -150f);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {

                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(50f, 7, -150f);

                        escaped = 0;
                        trapped = false;

                    }
                }
            }

            // living room trap
            if (playerObject.CharacterBody.Transform.Position.X < 50 && playerObject.CharacterBody.Transform.Position.X > 47
                && playerObject.CharacterBody.Transform.Position.Z < -83.5 && playerObject.CharacterBody.Transform.Position.Z > -86.5
                && playerObject.CharacterBody.Position.Y <= 7)
            {
                if (trapped == false)
                {
                    object[] parameters = { "bearTrap" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters));

                    object[] parameters2 = { "grunt4" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound,
                        EventActionType.OnPlay2D, parameters2));

                    trapped = true;
                }

                playerObject.CharacterBody.Position = new Vector3(48.5f, 4, -85f);

                if (KeyboardManager.IsFirstKeyPress(MoveKeys[6]) || GamePadManager.IsFirstButtonPress(PlayerIndex.One, Buttons.A))
                {

                    escaped++;

                    // press space bar 10 times to escape (3 loops = 1 press, so 3 x 10 = 30)
                    if (escaped == 30)
                    {
                        playerObject.CharacterBody.Position = new Vector3(48.5f, 7, -85f);

                        escaped = 0;
                        trapped = false;

                    }
                }
            }
        }

        //to do - clone, dispose
    }
}