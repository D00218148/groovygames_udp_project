﻿using GDLibrary;
using GDLibrary.Actors;
using GDLibrary.Controllers;
using GDLibrary.Core.Controllers;
using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.Factories;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using GDLibrary.Debug;
using JigLibX.Collision;
using JigLibX.Geometry;
using GDLibrary.Core.Utilities;
using Vector2 = Microsoft.Xna.Framework.Vector2;
using GDGame.Actors;
using GDGame.Controllers;
using GDLibrary.Containers;
using Microsoft.Xna.Framework.Media;
using GDGame.MyGame.Managers;

namespace GDGame
{
    public class Main : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private BasicEffect unlitTexturedEffect, unlitWireframeEffect;
        private CameraManager<Camera3D> cameraManager;
        private ObjectManager objectManager;
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private GamePadManager gamePadManager;
        private PhysicsManager physicsManager;
        private RenderManager renderManager;
        private SoundManager soundManager;
        private PickingManager pickingManager;
        private UIManager uiManager;
        //private MenuManager menuManager;
        private MyMenuManager menuManager;
        private GameStateManager gameStateManager;

        //store useful game resources (e.g. effects, models, rails and curves)
        private Dictionary<string, BasicEffect> effectDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;
        private ContentDictionary<Model> modelDictionary;

        private Dictionary<string, KillerTransform3DCurve> killerTransform3DCurveDictionary;
        private Dictionary<string, Transform3DCurve> transform3DCurveDictionary;
        private Dictionary<string, RailParameters> railDictionary;

        private EventDispatcher_OLD eventDispatcher;
        private EventDispatcher eventDispatcherV2;

        //eventually we will remove this content
        private VertexPositionColorTexture[] vertices;
        private Texture2D backSky, leftSky, rightSky, frontSky, topSky, grass, ground, crosshair;
        private Texture2D hallwayFloor;
        private Texture2D kitchenFloor;
        private Texture2D woodFloor1;
        private Texture2D woodFloor2;
        private Texture2D woodFloor3;
        private PrimitiveObject archetypalTexturedQuad;
        private float worldScale = 3000;
        private PrimitiveObject primitiveObject = null;
        private Microsoft.Xna.Framework.Vector2 screenCentre = Vector2.Zero;
        private BasicEffect modelEffect;
        private SpriteFont debugFont;
        private bool showDebug = false;
        private bool f1Held = false;
        private float mapLength = 100;
        private float mapWidth = 50;
        private float mapHeight = 40;
        private CollidableObject archetypalBoxObject;
        private KillerObject killerObject;
        private ModelObject archetypalBoxNAObject;
        private Texture2D woodFloor4;
        private Texture2D woodFloor5;
        private Texture2D woodFloor6;
        private Texture2D houseWall;
        private Texture2D houseWall2;

        private RandomKeyLocations randomKeyLocations;
        private Vector3 diamondKeyLocation;
        private Vector3 heartKeyLocation;
        private Vector3 spadeKeyLocation;

        private UITextureObject diamondHudIcon;
        private UITextureObject heartHudIcon;
        private UITextureObject spadeHudIcon;

        public Main()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
        }

        #region Debug
#if DEBUG
        private void InitDebug()
        {
            //InitDebugInfo(true);
            InitializeDebugCollisionSkinInfo(true);
        }

        private void InitDebugInfo(bool bEnable)
        {
            if (bEnable)
            {
                //create the debug drawer to draw debug info
                DebugDrawer debugInfoDrawer = new DebugDrawer(this, _spriteBatch, debugFont,
                    cameraManager, objectManager);

                //set the debug drawer to be drawn AFTER the object manager to the screen
                debugInfoDrawer.DrawOrder = 2;

                //add the debug drawer to the component list so that it will have its Update/Draw methods called each cycle.
                Components.Add(debugInfoDrawer);
            }
        }

        private void InitializeDebugCollisionSkinInfo(bool bEnable)
        {
            if (bEnable)
            {
                //show the collision skins
                PhysicsDebugDrawer physicsDebugDrawer = new PhysicsDebugDrawer(this, StatusType.Update | StatusType.Drawn,
                    cameraManager, objectManager);

                //set the debug drawer to be drawn AFTER the object manager to the screen
                physicsDebugDrawer.DrawOrder = 3;

                Components.Add(physicsDebugDrawer);


            }
        }

#endif
        #endregion Debug

        /// <summary>
        /// Initialises everything within the game.
        /// </summary>
        #region Initialization - Managers, Cameras, Effects, Textures
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Window.Title = "House Of Horror";

            InitGraphics(1024, 768);

            //note that we moved this from LoadContent to allow InitDebug to be called in Initialize
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            InitEventDispatcher();

            InitManagers();

            InitDictionaries();

            //initialize assets, effects and vertices
            LoadEffects();
            LoadTextures();
            LoadModels();

            InitVertices();
            InitTextures();
            InitAudio();
            InitFonts();
            InitDoorKeyLocations();

            //ui
            InitUI();
            InitMenu();

            InitDrawnContent();

            InitCollidableDrawnContent();

            InitCurves();

            InitCameras3D();
            InitKiller();


            base.Initialize();
        }

        /// <summary>
        /// Creates any required curves and adds them to a curve dictionary.
        /// </summary>
        private void InitCurves()
        {
            //create the killer path curve to be applied to the track controller
            KillerTransform3DCurve curveA = new KillerTransform3DCurve(CurveLoopType.Cycle);
            curveA.Add(new Vector3(0, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 0);
            curveA.Add(new Vector3(-20, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 1700);
            curveA.Add(new Vector3(-40, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 3400);
            curveA.Add(new Vector3(-65, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 5100);
            curveA.Add(new Vector3(-65, 3.4f, -142f), new Vector3(0, 0, 0), Vector3.UnitY, 5400);
            curveA.Add(new Vector3(-65, 3.4f, -115f), new Vector3(0, 0, 0), Vector3.UnitY, 7300);
            curveA.Add(new Vector3(-65, 3.4f, -115f), new Vector3(0, 90, 0), Vector3.UnitY, 7600);
            curveA.Add(new Vector3(-45, 3.4f, -115f), new Vector3(0, 90, 0), Vector3.UnitY, 9300);
            curveA.Add(new Vector3(-34, 3.4f, -115f), new Vector3(0, 90, 0), Vector3.UnitY, 10100);
            curveA.Add(new Vector3(-34, 3.4f, -115f), new Vector3(0, -90, 0), Vector3.UnitY, 10400);
            curveA.Add(new Vector3(-45, 3.4f, -115f), new Vector3(0, -90, 0), Vector3.UnitY, 11200);
            curveA.Add(new Vector3(-67.5f, 3.4f, -115f), new Vector3(0, -90, 0), Vector3.UnitY, 12900);
            curveA.Add(new Vector3(-67.5f, 3.4f, -115f), new Vector3(0, 0, 0), Vector3.UnitY, 13200);
            curveA.Add(new Vector3(-67.5f, 3.4f, -95f), new Vector3(0, 0, 0), Vector3.UnitY, 14900);
            curveA.Add(new Vector3(-67.5f, 3.4f, -80f), new Vector3(0, 0, 0), Vector3.UnitY, 16200);
            curveA.Add(new Vector3(-67.5f, 3.4f, -80f), new Vector3(0, 90, 0), Vector3.UnitY, 16500);
            curveA.Add(new Vector3(-47.5f, 3.4f, -80f), new Vector3(0, 90, 0), Vector3.UnitY, 18200);
            curveA.Add(new Vector3(-35f, 3.4f, -80f), new Vector3(0, 90, 0), Vector3.UnitY, 19000);
            curveA.Add(new Vector3(-35f, 3.4f, -80f), new Vector3(0, 0, 0), Vector3.UnitY, 19300);
            curveA.Add(new Vector3(-35f, 3.4f, -60f), new Vector3(0, 0, 0), Vector3.UnitY, 20900);
            curveA.Add(new Vector3(-35f, 3.4f, -45f), new Vector3(0, 0, 0), Vector3.UnitY, 22000);
            curveA.Add(new Vector3(-35f, 3.4f, -45f), new Vector3(0, -90, 0), Vector3.UnitY, 22300);
            curveA.Add(new Vector3(-55f, 3.4f, -45f), new Vector3(0, -90, 0), Vector3.UnitY, 23800);
            curveA.Add(new Vector3(-55f, 3.4f, -45f), new Vector3(0, 90, 0), Vector3.UnitY, 24100);
            curveA.Add(new Vector3(-35f, 3.4f, -45f), new Vector3(0, 90, 0), Vector3.UnitY, 25700);
            curveA.Add(new Vector3(-35f, 3.4f, -45f), new Vector3(0, 180, 0), Vector3.UnitY, 26000);
            curveA.Add(new Vector3(-35f, 3.4f, -57f), new Vector3(0, 180, 0), Vector3.UnitY, 26800);
            curveA.Add(new Vector3(-35f, 3.4f, -57f), new Vector3(0, 90, 0), Vector3.UnitY, 27100);
            curveA.Add(new Vector3(-15f, 3.4f, -57.5f), new Vector3(0, 90, 0), Vector3.UnitY, 28700);
            curveA.Add(new Vector3(5f, 3.4f, -57.5f), new Vector3(0, 90, 0), Vector3.UnitY, 30300);
            curveA.Add(new Vector3(25f, 3.4f, -57.5f), new Vector3(0, 90, 0), Vector3.UnitY, 31900);
            curveA.Add(new Vector3(39f, 3.4f, -57.5f), new Vector3(0, 90, 0), Vector3.UnitY, 32700);
            curveA.Add(new Vector3(39f, 3.4f, -57.5f), new Vector3(0, 0, 0), Vector3.UnitY, 33000);
            curveA.Add(new Vector3(39f, 3.4f, -44f), new Vector3(0, 0, 0), Vector3.UnitY, 33800);
            curveA.Add(new Vector3(39f, 3.4f, -44f), new Vector3(0, 90, 0), Vector3.UnitY, 34100);
            curveA.Add(new Vector3(62f, 3.4f, -44f), new Vector3(0, 90, 0), Vector3.UnitY, 35700);
            curveA.Add(new Vector3(62f, 3.4f, -44f), new Vector3(0, 180, 0), Vector3.UnitY, 36000);
            curveA.Add(new Vector3(62f, 3.4f, -69.5f), new Vector3(0, 180, 0), Vector3.UnitY, 37700);
            curveA.Add(new Vector3(62f, 3.4f, -69.5f), new Vector3(0, 210, 0), Vector3.UnitY, 37800);
            curveA.Add(new Vector3(39f, 3.4f, -89.5f), new Vector3(0, 210, 0), Vector3.UnitY, 39500);
            curveA.Add(new Vector3(33.5f, 3.4f, -94f), new Vector3(0, 210, 0), Vector3.UnitY, 40300);
            curveA.Add(new Vector3(33.5f, 3.4f, -94f), new Vector3(0, 180, 0), Vector3.UnitY, 40400);
            curveA.Add(new Vector3(33.5f, 3.4f, -102f), new Vector3(0, 167, 0), Vector3.UnitY, 40800);
            curveA.Add(new Vector3(49f, 3.4f, -142f), new Vector3(0, 146, 0), Vector3.UnitY, 42400);
            curveA.Add(new Vector3(49f, 3.4f, -142f), new Vector3(0, 90, 0), Vector3.UnitY, 42700);
            curveA.Add(new Vector3(64f, 3.4f, -142f), new Vector3(0, 90, 0), Vector3.UnitY, 43700);
            curveA.Add(new Vector3(64f, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 44000);
            curveA.Add(new Vector3(44f, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 45600);
            curveA.Add(new Vector3(24f, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 47100);
            curveA.Add(new Vector3(0, 3.4f, -142f), new Vector3(0, -90, 0), Vector3.UnitY, 48700);

            //add to the dictionary
            killerTransform3DCurveDictionary.Add("killer route", curveA);
        }

        /// <summary>
        /// Initialises any drawn content that can be collided with.
        /// </summary>
        private void InitCollidableDrawnContent()
        {
            InitStaticCollidableGround();
        }

        /// <summary>
        /// Initialises the ground in the level.
        /// </summary>
        private void InitStaticCollidableGround()
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;
            Model model = null;

            model = Content.Load<Model>("Assets/Models/box2");

            effectParameters = new EffectParameters(modelEffect,
                  Content.Load<Texture2D>("Assets/Textures/Foliage/Ground/grass1"),
                  Color.White, 1);

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(worldScale, 0.001f, worldScale), -Vector3.UnitZ, Vector3.UnitY);

            collidableObject = new CollidableObject("ground", ActorType.CollidableGround,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, model);

            //focus on CDCR specific methods and parameters - plane, sphere, box, capsule
            collidableObject.AddPrimitive(new JigLibX.Geometry.Plane(transform3D.Up, transform3D.Translation),
                new MaterialProperties(0.8f, 0.8f, 0.7f));

            collidableObject.Enable(true, 1); //change to false, see what happens.

            objectManager.Add(collidableObject);
        }

        /// <summary>
        /// Initialises any dictionaries that are needed, e.g. modelDictionary to store models.
        /// </summary>
        private void InitDictionaries()
        {
            //stores effects
            effectDictionary = new Dictionary<string, BasicEffect>();

            //stores textures, fonts & models
            modelDictionary = new ContentDictionary<Model>("models", Content);
            textureDictionary = new ContentDictionary<Texture2D>("textures", Content);
            fontDictionary = new ContentDictionary<SpriteFont>("fonts", Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            transform3DCurveDictionary = new Dictionary<string, Transform3DCurve>();
            killerTransform3DCurveDictionary = new Dictionary<string, KillerTransform3DCurve>();

            //rails - store rails used by cameras
            railDictionary = new Dictionary<string, RailParameters>();
        }

        /// <summary>
        /// Randomises a set of locations for the door key objects.
        /// </summary>
        private void InitDoorKeyLocations()
        {
            //random key locations

            this.randomKeyLocations = new RandomKeyLocations();
            this.diamondKeyLocation = randomKeyLocations.DiamondKeyLocation;
            this.heartKeyLocation = randomKeyLocations.HeartKeyLocation;
            this.spadeKeyLocation = randomKeyLocations.SpadeKeyLocation;

            this.randomKeyLocations.printAllLocations();
            this.randomKeyLocations.printDoorKeyLocations();
        }

        /// <summary>
        /// Initialises the menu in the game and any buttons that are part of it.
        /// </summary>
        private void InitMenu()
        {
            Texture2D texture = null;
            Transform2D transform2D = null;
            DrawnActor2D uiObject = null;
            Vector2 fullScreenScaleFactor = Vector2.One;

            #region All Menu Background Images
            //background main
            texture = textureDictionary["menuBG"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);

            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("main_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.AntiqueWhite, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("main", uiObject);

            //background audio
            texture = textureDictionary["audiomenu"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("audio_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("audio", uiObject);

            //background "how to play"
            texture = textureDictionary["howToPlay"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("howToPlay_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("howToPlay", uiObject);

            //background controls
            texture = textureDictionary["controls"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("controls_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("controls", uiObject);

            //background credits
            texture = textureDictionary["credits"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("credits_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("credits", uiObject);

            //background win 
            texture = textureDictionary["winScene"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("win_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("winScene", uiObject);

            //background lose
            texture = textureDictionary["loseScene"];
            fullScreenScaleFactor = new Vector2((float)_graphics.PreferredBackBufferWidth / texture.Width, (float)_graphics.PreferredBackBufferHeight / texture.Height);
            transform2D = new Transform2D(fullScreenScaleFactor);
            uiObject = new UITextureObject("lose_bckgnd", ActorType.UITextureObject, StatusType.Drawn,
                transform2D, Color.White, 1, SpriteEffects.None, texture, new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));
            menuManager.Add("loseScene", uiObject);

            #endregion All Menu Background Images

            //main menu buttons
            texture = textureDictionary["genericbtn"];

            Vector2 origin = new Vector2(texture.Width / 2, texture.Height / 2);

            Integer2 imageDimensions = new Integer2(texture.Width, texture.Height);

            //play
            transform2D = new Transform2D(screenCentre - new Vector2(0, 0), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("play", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "Play",
                fontDictionary["menu"],
                new Vector2(1, 1),
                Color.Black,
                new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Goldenrod, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.01f, .5f, 0)));

            menuManager.Add("main", uiObject);

            //How To Play
            transform2D = new Transform2D(screenCentre + new Vector2(0, 70), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("howToPlay", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "howToPlay",
                fontDictionary["menu"],
                new Vector2(1, 1),
                Color.Black,
                new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Goldenrod, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.01f, .5f, 0)));

            menuManager.Add("main", uiObject);

            //controls
            transform2D = new Transform2D(screenCentre + new Vector2(0, 140), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("controls", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "Controls",
                fontDictionary["menu"],
                new Vector2(1, 1),
                Color.Black,
                new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Goldenrod, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.01f, .5f, 0)));

            menuManager.Add("main", uiObject);

            //credits
            transform2D = new Transform2D(screenCentre + new Vector2(0, 210), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("credits", ActorType.UITextureObject, StatusType.Drawn | StatusType.Update,
                transform2D, Color.White, 1, SpriteEffects.None, texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
                "Credits",
                fontDictionary["menu"],
                new Vector2(1, 1),
                Color.Black,
                new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.Goldenrod, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.01f, .5f, 0)));

            menuManager.Add("main", uiObject);

            //exit
            transform2D = new Transform2D(screenCentre + new Vector2(0, 280), 0, Vector2.One, origin, imageDimensions);
            uiObject = new UIButtonObject("exit", ActorType.UITextureObject,
                StatusType.Update | StatusType.Drawn,
             transform2D, Color.White, 1, SpriteEffects.None, texture,
             new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height),
             "Exit",
             fontDictionary["menu"],
             new Vector2(1, 1),
             Color.Black,
             new Vector2(0, 0));

            uiObject.ControllerList.Add(new UIMouseOverController("moc1", ControllerType.MouseOver,
                 mouseManager, Color.DarkRed, Color.White));

            uiObject.ControllerList.Add(new UIScaleLerpController("slc1", ControllerType.ScaleLerpOverTime,
              mouseManager, new TrigonometricParameters(0.01f, .5f, 0)));

            menuManager.Add("main", uiObject);

            //finally dont forget to SetScene to say which menu should be drawn/updated!
            menuManager.SetScene("main");
        }

        /// <summary>
        /// Initialises the event dispatcher which handles the subscription and publishing of events.
        /// </summary>
        private void InitEventDispatcher()
        {
            //this.eventDispatcher = new EventDispatcher_OLD(this);
            //Components.Add(this.eventDispatcher);

            this.eventDispatcherV2 = new EventDispatcher(this);
            Components.Add(this.eventDispatcherV2);
        }

        /// <summary>
        /// Removes the debug component.
        /// </summary>
        private void RemoveDebug()
        {
            // debug component is added at position 4
            //Components.RemoveAt(13);
            Components.RemoveAt(12);
        }

        /// <summary>
        /// Initialises any font that will be used for the ui, menu, etc.
        /// </summary>
        private void InitFonts()
        {
            this.debugFont = Content.Load<SpriteFont>("Assets/Fonts/debug");
            fontDictionary.Load("Assets/Fonts/ui");
            fontDictionary.Load("Assets/Fonts/menu");
        }

        /// <summary>
        /// Initialises managers and adds them to a collection of game components.
        /// </summary>
        private void InitManagers()
        {
            //physics and CD-CR (moved to top because MouseManager is dependent)
            physicsManager = new PhysicsManager(this, StatusType.Update, -9.81f * Vector3.UnitY);
            Components.Add(physicsManager);

            //camera
            cameraManager = new CameraManager<Camera3D>(this, StatusType.Update);
            Components.Add(cameraManager);

            //keyboard
            keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager);

            //mouse
            mouseManager = new MouseManager(this, true, physicsManager, screenCentre);
            Components.Add(mouseManager);

            //gamepad
            gamePadManager = new GamePadManager(this, 1);
            Components.Add(gamePadManager);

            //object
            objectManager = new ObjectManager(this, StatusType.Update, 6, 10);
            Components.Add(objectManager);

            //render
            renderManager = new RenderManager(this, StatusType.Drawn, ScreenLayoutType.Single,
                objectManager, cameraManager);
            Components.Add(renderManager);

            //picking
            Predicate<CollidableObject> collisionPredicate
                    = (collidableObject) =>
                    {
                        if (collidableObject != null)
                        {
                            if (collidableObject.ActorType == ActorType.CollidableInventory)
                            {
                                if (collidableObject.ID == "Diamond key")
                                {
                                    System.Diagnostics.Debug.WriteLine("Diamond Icon");
                                    this.diamondHudIcon.StatusType = StatusType.Drawn | StatusType.Update;
                                }
                                else if (collidableObject.ID == "Heart Key")
                                {
                                    System.Diagnostics.Debug.WriteLine("Heart Key");
                                    this.heartHudIcon.StatusType = StatusType.Drawn | StatusType.Update;
                                }
                                else if (collidableObject.ID == "Spade Key")
                                {
                                    System.Diagnostics.Debug.WriteLine("Spade Key");
                                    this.spadeHudIcon.StatusType = StatusType.Drawn | StatusType.Update;
                                }
                            }
                            return collidableObject.ActorType == ActorType.CollidableInventory
                            || collidableObject.ActorType == ActorType.CollidablePickup;
                        }
                        else
                        {
                            return false;
                        }
                    };

            pickingManager = new PickingManager(this, StatusType.Update, keyboardManager, mouseManager,
                gamePadManager, cameraManager, GameConstants.CollidableCameraCapsuleRadius * 1.5f, 1000,
                collisionPredicate);
            Components.Add(pickingManager);

            //add in-game ui
            uiManager = new UIManager(this, StatusType.Drawn | StatusType.Update, _spriteBatch, 10);
            uiManager.DrawOrder = 4;
            Components.Add(uiManager);

            //add menu
            menuManager = new MyMenuManager(this, StatusType.Update | StatusType.Drawn, _spriteBatch,
                mouseManager, keyboardManager);
            menuManager.DrawOrder = 5; //highest number of all drawable managers since we want it drawn on top!
            Components.Add(menuManager);

            soundManager = new SoundManager(this, StatusType.Update);
            Components.Add(soundManager);

            gameStateManager = new GameStateManager(this, StatusType.Update, menuManager);
            Components.Add(gameStateManager);
        }

        /// <summary>
        /// Initialises cameras needed for the game.
        /// </summary>
        private void InitCameras3D()
        {

            Transform3D transform3D = null;
            Camera3D camera3D = null;
            Viewport viewPort = new Viewport(0, 0, 1024, 768);

            #region Collidable First Person Camera  

            transform3D = new Transform3D(new Vector3(0, 8, -45),
                new Vector3(0, 0, -1), Vector3.UnitY);

            camera3D = new Camera3D("Collidable 1st person",
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, new Viewport(0, 0, 1024, 768));

            //attach a controller
            camera3D.ControllerList.Add(new CollidableFirstPersonCameraController("cfpcc",
                ControllerType.FirstPersonCollidable,
                this.keyboardManager, this.mouseManager, this.gamePadManager, this.gameStateManager,
                GameConstants.CameraMoveKeys,
                GameConstants.CollidableCameraMoveSpeed,
                GameConstants.CollidableCameraStrafeSpeed,
                GameConstants.rotateSpeed,
                camera3D,
                new Vector3(0, 2, 0), //translation offset
                GameConstants.CollidableCameraCapsuleRadius,
                GameConstants.CollidableCameraViewHeight,
                2, 2, //accel, decel
                GameConstants.CollidableCameraMass,
                GameConstants.CollidableCameraJumpHeight));

            cameraManager.Add(camera3D);

            #endregion Collidable First Person Camera

            #region Camera - Debug First Person
            transform3D = new Transform3D(new Vector3(0, 10, 0),
                new Vector3(0, 0, -1), Vector3.UnitY);

            Camera3D camera3D2 = new Camera3D("Debug 1st person",
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, new Viewport(5, 7, 502, 374));

            //attach a controller
            camera3D2.ControllerList.Add(new DebugFirstPersonCameraController(
                this.keyboardManager, this.mouseManager,
                GameConstants.debugMoveSpeed, GameConstants.debugStrafeSpeed, GameConstants.scrollSpeed, GameConstants.rotateSpeed));
            this.cameraManager.Add(camera3D2);
            #endregion

            #region Camera - Birds Eye View Far
            transform3D = new Transform3D(new Vector3(0, 200, -100),
                       new Vector3(0, -1, 0), //look
                       new Vector3(0, -1, -1)); //up

            this.cameraManager.Add(new Camera3D("Birds eye view far",
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, new Viewport(517, 7, 502, 374)));

            this.cameraManager.Add(camera3D);
            #endregion

            #region Camera - Birds Eye View Close
            transform3D = new Transform3D(new Vector3(0, 50, 0),
                       new Vector3(0, -1, 0), //look
                       new Vector3(0, -1, -1)); //up

            this.cameraManager.Add(new Camera3D("Birds eye view close",
                ActorType.Camera3D, StatusType.Update, transform3D,
                ProjectionParameters.StandardDeepSixteenTen, new Viewport(5, 386, 502, 374)));

            this.cameraManager.Add(camera3D);
            #endregion


        }

        /// <summary>
        /// Initialises textures for the skybox, the walls and the floors.
        /// </summary>
        private void InitTextures()
        {
            //step 1 - texture
            this.backSky
                = Content.Load<Texture2D>("Assets/Textures/Skybox/backNightSky");
            this.leftSky
               = Content.Load<Texture2D>("Assets/Textures/Skybox/leftNightSky");
            this.rightSky
              = Content.Load<Texture2D>("Assets/Textures/Skybox/rightNightSky");
            this.frontSky
              = Content.Load<Texture2D>("Assets/Textures/Skybox/frontNightSky");
            this.topSky
              = Content.Load<Texture2D>("Assets/Textures/Skybox/topNightSky");
            this.ground
                = Content.Load<Texture2D>("Assets/Textures/Foliage/Ground/grass1");

            this.crosshair
                = Content.Load<Texture2D>("Assets/Textures/Player/Crosshairs/crossHair");

            this.hallwayFloor = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/CarpetTexture");
            this.kitchenFloor = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyTiles");
            this.woodFloor1 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood");
            this.woodFloor2 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood2");
            this.woodFloor3 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood3");
            this.woodFloor4 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood4");
            this.woodFloor5 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood5");
            this.woodFloor6 = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood6");

            this.houseWall = Content.Load<Texture2D>("Assets/Textures/Architecture/Walls/DirtyWall");
            this.houseWall2 = Content.Load<Texture2D>("Assets/Textures/Architecture/Walls/DirtyWall2");

        }

        /// <summary>
        /// creates a scheduler for dialogue files and has them played after every 15 seconds.
        /// </summary>
        private void InitDialogue()
        {
            EventScheduler scheduler = new EventScheduler("Killer Dialogue and ambience");

            object[] parameters = { "closerangry" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters), 15000);

            object[] parameters2 = { "goingtogetyousad" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2), 30000);

            object[] parameters3 = { "ambience1" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters3), 45000);

            object[] parameters4 = { "catchyouhappy" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters4), 60000);

            object[] parameters5 = { "canthidesad" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters5), 75000);

            object[] parameters6 = { "gonnadieangry" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters6), 90000);

            object[] parameters7 = { "closerhappy" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters7), 105000);

            object[] parameters8 = { "ambience2" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters8), 120000);

            object[] parameters9 = { "gonnadiehappy" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters9), 135000);

            object[] parameters10 = { "catchyousad" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters10), 150000);

            object[] parameters11 = { "goingtogetyouangry" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters11), 165000);

            object[] parameters12 = { "canthidehappy" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters12), 180000);

            object[] parameters13 = { "catchyouangry" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters13), 195000);

            object[] parameters14 = { "closersad" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters14), 210000);

            object[] parameters15 = { "goingtogetyouhappy" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters15), 225000);

            object[] parameters16 = { "canthideangry" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters16), 240000);

            object[] parameters17 = { "gonnadieasad" };
            scheduler.Add(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters17), 255000);

            scheduler.Start();
        }

        /// <summary>
        /// Initialises audio for the game and adds all of the sounds into the sound manager.
        /// </summary>
        private void InitAudio()
        {
            //music and SFX
            this.soundManager.Add(new GDLibrary.Managers.Cue("keypickup", Content.Load<SoundEffect>("Assets/Audio/SFX/KeyPickup"), SoundCategoryType.WinLose, new Vector3(0.4f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("gametheme", Content.Load<SoundEffect>("Assets/Audio/Music/GameThemeRemastered"), SoundCategoryType.BackgroundMusic, new Vector3(0.1f, 0, 0), true));

            this.soundManager.Add(new GDLibrary.Managers.Cue("menutheme", Content.Load<SoundEffect>("Assets/Audio/Music/Menu theme"), SoundCategoryType.BackgroundMusic, new Vector3(0.3f, 0, 0), true));

            this.soundManager.Add(new GDLibrary.Managers.Cue("bearTrap", Content.Load<SoundEffect>("Assets/Audio/SFX/Bear trap"), SoundCategoryType.Trap, new Vector3(0.1f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("grunt4", Content.Load<SoundEffect>("Assets/Audio/SFX/grunt4"), SoundCategoryType.Hurt, new Vector3(0.1f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("contactwithkiller", Content.Load<SoundEffect>("Assets/Audio/SFX/Contact with killer edited"), SoundCategoryType.WinLose, new Vector3(0.143f, 0, 0), false));

            this.soundManager.Add(new GDLibrary.Managers.Cue("wingrandpiano", Content.Load<SoundEffect>("Assets/Audio/SFX/wingrandpiano"), SoundCategoryType.WinLose, new Vector3(0.5f, 0, 0), false));

            //dialogue
            this.soundManager.Add(new GDLibrary.Managers.Cue("closerangry", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGettingCloserAngry"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("goingtogetyousad", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGoingToGetYouSad"), SoundCategoryType.Dialogue, new Vector3(0.15f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("catchyouhappy", Content.Load<SoundEffect>("Assets/Audio/Dialogue/IWillCatchYouHappy"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("canthidesad", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YouCanRunButYouCantHideSad"), SoundCategoryType.Dialogue, new Vector3(0.15f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("gonnadieangry", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YoureGonnaDieAngry"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("closerhappy", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGettingCloserHappy"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("gonnadiehappy", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YoureGonnaDieHappy"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("catchyousad", Content.Load<SoundEffect>("Assets/Audio/Dialogue/IWillCatchYouSad"), SoundCategoryType.Dialogue, new Vector3(0.15f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("goingtogetyouangry", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGoingToGetYouAngry"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("canthidehappy", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YouCanRunButYouCantHideHappy"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("closersad", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGettingCloserSad"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("goingtogetyouhappy", Content.Load<SoundEffect>("Assets/Audio/Dialogue/ImGoingToGetYouHappy"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("catchyouangry", Content.Load<SoundEffect>("Assets/Audio/Dialogue/IWillCatchYouAngry"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("canthideangry", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YouCanRunButYouCantHideAngry"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("gonnadieasad", Content.Load<SoundEffect>("Assets/Audio/Dialogue/YoureGonnaDieSad"), SoundCategoryType.Dialogue, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("ambience1", Content.Load<SoundEffect>("Assets/Audio/Ambience/ambience1"), SoundCategoryType.Ambience, new Vector3(0.1f, 0, 0), false));
            this.soundManager.Add(new GDLibrary.Managers.Cue("ambience2", Content.Load<SoundEffect>("Assets/Audio/Ambience/ambience2"), SoundCategoryType.Ambience, new Vector3(0.065f, 0, 0), false));

            InitDialogue();
        }
        #endregion

        /// <summary>
        /// Initialises all drawn content in the game.
        /// </summary>
        #region Initialization - Vertices, Archetypes, Helpers, Drawn Content(e.g. Skybox)
        private void InitDrawnContent() //formerly InitPrimitives
        {
            //add archetypes that can be cloned
            InitPrimitiveArchetypes();

            //adds origin helper etc
            InitHelpers();

            //add skybox
            InitSkybox();

            //add grass plane
            InitGround();

            //models
            InitStaticModels();

            //Game level
            InitGameLevel();

            InitGameMusic();

            InitKeys();
        }

        /// <summary>
        /// Initialises the menu theme music.
        /// </summary>
        private void InitGameMusic()
        {
            object[] parameters = { "menutheme" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));
        }

        /// <summary>
        /// Initialises an archetypalBoxObject which can be reused and adds it to the object manager.
        /// </summary>
        private void InitStaticModels()
        {
            //transform
            Transform3D transform3D = new Transform3D(new Vector3(0, 2, 100),
                                new Vector3(0, 0, 0),       //rotation
                                new Vector3(1, 1, 1),        //scale
                                    -Vector3.UnitZ,         //look
                                    Vector3.UnitY);         //up

            //effectparameters
            EffectParameters effectParameters = new EffectParameters(modelEffect,
                Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood"),
                Color.White, 1);

            //model
            Model model = Content.Load<Model>("Assets/Models/box2");

            //Collidable box object
            archetypalBoxObject = new TriangleMeshObject("box", ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, transform3D, effectParameters, model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            archetypalBoxObject.Enable(true, 1);

            this.objectManager.Add(archetypalBoxObject);
        }

        /// <summary>
        /// This method is used to intialise the game level(walls, floors, furniture)
        /// </summary>
        private void InitGameLevel()
        {
            CollidableObject modelObject = null;
            PickupCollidableObject keyObject = null;

            #region Walls
            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Back Wall";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(60, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(0, -5, -36);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Front Wall";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(60, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(0, -5, -163.5f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Left Wall";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(50, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-76.5f, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Right Wall";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(50, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(76.5f, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Right";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(30, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(25, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Right2";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(7, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(25, -5, -45);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Right3";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(7, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(25, -5, -155);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Left";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(30, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-25, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Left2";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(7, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-25, -5, -45);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Room Separating Wall Left";
            modelObject.EffectParameters.Texture = this.houseWall2;
            modelObject.Transform3D.Scale = new Vector3(15, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(-44, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Room Separating Wall Left2";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(2, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(-74.5f, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Wall Left3";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(7, 20, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-25, -5, -155);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Room Separating Wall Right";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(15, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(57, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Room Separating Wall Right2";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(2, 20, 0.5f);
            modelObject.Transform3D.Translation = new Vector3(27, -5, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Roof";
            modelObject.EffectParameters.Texture = this.houseWall;
            modelObject.Transform3D.Scale = new Vector3(60, 50, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);
            modelObject.Transform3D.Translation = new Vector3(0, 20, -100);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);
            #endregion Walls

            #region Floors
            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen Floor";
            modelObject.EffectParameters.Texture = this.kitchenFloor;
            modelObject.Transform3D.Scale = new Vector3(20, 10, 25);
            modelObject.Transform3D.Translation = new Vector3(-50.7f, -10, -131.5f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1); Causes CollidableFirstPerson to "jump around" while moving along the floor
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bedroom Floor";
            modelObject.EffectParameters.Texture = this.woodFloor2;
            modelObject.Transform3D.Scale = new Vector3(20, 10, 25);
            modelObject.Transform3D.Translation = new Vector3(50.7f, -10, -131.5f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Living Room Floor";
            modelObject.EffectParameters.Texture = this.woodFloor1;
            modelObject.Transform3D.Scale = new Vector3(20, 10, 25);
            modelObject.Transform3D.Translation = new Vector3(50.7f, -10, -68);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hallway Floor";
            modelObject.EffectParameters.Texture = this.hallwayFloor;
            modelObject.Transform3D.Scale = new Vector3(20, 10, 50);
            modelObject.Transform3D.Translation = new Vector3(0, -10, -99.75f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Floor";
            modelObject.EffectParameters.Texture = Content.Load<Texture2D>("Assets/Textures/Architecture/Floors/DirtyWood4");
            modelObject.Transform3D.Scale = new Vector3(20, 10, 25);
            modelObject.Transform3D.Translation = new Vector3(-50.7f, -10, -68);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);
            #endregion

            #region DINING ROOM /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bookshelf 1";
            modelObject.Model = modelDictionary["hoh_bookshelf"];
            modelObject.EffectParameters.Texture = textureDictionary["SHELF_TEXTURE"];
            modelObject.Transform3D.Scale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            modelObject.Transform3D.Translation = new Vector3(-43f, 2f, -99.4f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bookshelf 2";
            modelObject.Model = modelDictionary["hoh_bookshelf"];
            modelObject.EffectParameters.Texture = textureDictionary["SHELF_TEXTURE"];
            modelObject.Transform3D.Scale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            modelObject.Transform3D.Translation = new Vector3(-38f, 2f, -99.4f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bookshelf 3";
            modelObject.Model = modelDictionary["hoh_bookshelf"];
            modelObject.EffectParameters.Texture = textureDictionary["SHELF_TEXTURE"];
            modelObject.Transform3D.Scale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            modelObject.Transform3D.Translation = new Vector3(-33f, 2f, -99.4f);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Chair 1";
            modelObject.Model = modelDictionary["hoh_diningChair"];
            modelObject.EffectParameters.Texture = textureDictionary["Chair"];
            modelObject.Transform3D.Scale = new Vector3(0.036f, 0.036f, 0.036f);
            modelObject.Transform3D.Translation = new Vector3(-46.5f, 2f, -65);
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Chair 2";
            modelObject.Model = modelDictionary["hoh_diningChair"];
            modelObject.EffectParameters.Texture = textureDictionary["Chair"];
            modelObject.Transform3D.Scale = new Vector3(0.036f, 0.036f, 0.036f);
            modelObject.Transform3D.Translation = new Vector3(-46f, 2f, -55);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Chair 3";
            modelObject.Model = modelDictionary["hoh_diningChair"];
            modelObject.EffectParameters.Texture = textureDictionary["Chair"];
            modelObject.Transform3D.Scale = new Vector3(0.036f, 0.036f, 0.036f);
            modelObject.Transform3D.Translation = new Vector3(-43f, 2f, -60);
            modelObject.Transform3D.RotateBy(new Vector3(0, 270, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Chair 4";
            modelObject.Model = modelDictionary["hoh_diningChair"];
            modelObject.EffectParameters.Texture = textureDictionary["Chair"];
            modelObject.Transform3D.Scale = new Vector3(0.036f, 0.036f, 0.036f);
            modelObject.Transform3D.Translation = new Vector3(-50f, 2f, -60);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Dining Table";
            modelObject.Model = modelDictionary["hoh_dining table"];
            modelObject.EffectParameters.Texture = textureDictionary["0"];
            modelObject.Transform3D.Scale = new Vector3(0.05f, 0.05f, 0.05f);
            modelObject.Transform3D.Translation = new Vector3(-46.5f, 5f, -60);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(-36f, 2.47f, -85f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(-45f, 2.47f, -46f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            #endregion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            #region KITCHEN ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen table";
            modelObject.Model = modelDictionary["hoh_kitchen table"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.00065f, 0.00065f, 0.00065f);
            modelObject.Transform3D.Translation = new Vector3(-33f, 5.5f, -122.7f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen table 2";
            modelObject.Model = modelDictionary["hoh_kitchen table"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.00065f, 0.00065f, 0.00065f);
            modelObject.Transform3D.Translation = new Vector3(-44f, 5.5f, -122.7f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen table 3";
            modelObject.Model = modelDictionary["hoh_kitchen table"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.00065f, 0.00065f, 0.00065f);
            modelObject.Transform3D.Translation = new Vector3(-33f, 5.5f, -128.5f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen table 4";
            modelObject.Model = modelDictionary["hoh_kitchen table"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.00065f, 0.00065f, 0.00065f);
            modelObject.Transform3D.Translation = new Vector3(-44f, 5.5f, -128.5f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-44f, 2.7f, -161.45f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter 2";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-58.7f, 2.7f, -161.45f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter 3";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-40f, 2.7f, -102.6f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter 4";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-43.7f, 2.7f, -102.6f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter 5";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-47.4f, 2.7f, -102.6f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Counter 6";
            modelObject.Model = modelDictionary["hoh_counter"];
            modelObject.EffectParameters.Texture = textureDictionary["0088-dark-raw-wood-texture-seamless-hr"];
            modelObject.Transform3D.Scale = new Vector3(0.045f, 0.045f, 0.045f);
            modelObject.Transform3D.Translation = new Vector3(-51.1f, 2.7f, -102.6f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen counters";
            modelObject.Model = modelObject.Model = modelDictionary["hoh_kitchen counters"];
            modelObject.EffectParameters.Texture = textureDictionary["kitchen_main_stand_light_map"];
            modelObject.Transform3D.Scale = new Vector3(0.0044f, 0.0044f, 0.0044f); //0.18f
            modelObject.Transform3D.Translation = new Vector3(-50f, 2.5f, -161.5f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Fridge";
            modelObject.Model = modelDictionary["hoh_fridge"];
            modelObject.EffectParameters.Texture = textureDictionary["fridgeDiffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.02f, 0.02f, 0.02f);
            modelObject.Transform3D.Translation = new Vector3(-31.5f, 2f, -102.6f);
            modelObject.Transform3D.RotateBy(new Vector3(90, 180, 0));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Kitchen Knife";
            modelObject.Model = modelDictionary["hoh_knife"];
            modelObject.EffectParameters.Texture = textureDictionary["Knife_Diffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.12f, 0.12f, 0.12f);
            modelObject.Transform3D.Translation = new Vector3(-39f, 6.1f, -127.2f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 90));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f)); ADDING THIS TO THE KNIFE CAUSES GAME TO DROP FRAMES
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Teapot";
            modelObject.Model = modelDictionary["hoh_teapot"];
            modelObject.EffectParameters.Texture = textureDictionary["Metal Seamless Texture #4116"];
            modelObject.EffectParameters.DiffuseColor = Color.DarkGray;
            modelObject.EffectParameters.Alpha = 0.7f;
            modelObject.Transform3D.Scale = new Vector3(1.5f, 1.5f, 1.5f);
            modelObject.Transform3D.Translation = new Vector3(-52f, 8.25f, -161f);
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f)); CAUSES FRAMES TO DROP
            //modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(-58f, 2.47f, -146f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(-38f, 2.47f, -120f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            #endregion ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            #region BEDROOM ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bookshelf 4";
            modelObject.Model = modelDictionary["hoh_bookshelf"];
            modelObject.EffectParameters.Texture = textureDictionary["SHELF_TEXTURE"];
            modelObject.Transform3D.Scale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            modelObject.Transform3D.Translation = new Vector3(65f, 2f, -100f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Candlestick 2";
            modelObject.Model = modelDictionary["hoh_candlestick"];
            modelObject.EffectParameters.Texture = textureDictionary["Metal Seamless Texture #4116"];
            modelObject.Transform3D.Scale = new Vector3(0.0015f, 0.0015f, 0.0015f);
            modelObject.Transform3D.Translation = new Vector3(73.5f, 6.2f, -140.5f);
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bedside Cabinet";
            modelObject.Model = modelDictionary["hoh_bedside table"];
            modelObject.EffectParameters.Texture = textureDictionary["diffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.0225f, 0.0225f, 0.0225f);
            modelObject.Transform3D.Translation = new Vector3(73f, 2.5f, -140);
            modelObject.Transform3D.RotateBy(new Vector3(0, -90, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bedside Cabinet 2";
            modelObject.Model = modelDictionary["hoh_bedside table"];
            modelObject.EffectParameters.Texture = textureDictionary["diffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.0225f, 0.0225f, 0.0225f);
            modelObject.Transform3D.Translation = new Vector3(74f, 2.5f, -126.5f);
            modelObject.Transform3D.RotateBy(new Vector3(0, -90, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bed";
            modelObject.Model = modelDictionary["hoh_bed"];
            modelObject.EffectParameters.Texture = textureDictionary["bedDiffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.06f, 0.05f, 0.05f);
            modelObject.Transform3D.Translation = new Vector3(67.49f, 3f, -135f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Desk";
            modelObject.Model = modelDictionary["hoh_desk"];
            modelObject.EffectParameters.Texture = textureDictionary["07_-_Default_Base_Color"];
            modelObject.Transform3D.Scale = new Vector3(0.032f, 0.032f, 0.032f);
            modelObject.Transform3D.Translation = new Vector3(50f, 2.5f, -163f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Cabinet";
            modelObject.Model = modelDictionary["hoh_cabinet"];
            modelObject.EffectParameters.Texture = textureDictionary["cabinet_Cabinet_BaseColor"];
            modelObject.Transform3D.Scale = new Vector3(0.035f, 0.035f, 0.035f);
            modelObject.Transform3D.Translation = new Vector3(50f, 2.5f, -101.68f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 180, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(50f, 2.47f, -150f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 90, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            #endregion ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            #region LIVING ROOM ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Tv Table";
            modelObject.Model = modelDictionary["hoh_tvTable"];
            modelObject.EffectParameters.Texture = textureDictionary["Wood028_4K_Color"];
            modelObject.Transform3D.Scale = new Vector3(0.07f, 0.07f, 0.07f);
            modelObject.Transform3D.Translation = new Vector3(70f, 2, -42);
            modelObject.Transform3D.RotateBy(new Vector3(0, 45, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Old Tv";
            modelObject.Model = modelDictionary["hoh_Tv"];
            modelObject.EffectParameters.Texture = textureDictionary["TV_Color"];
            modelObject.Transform3D.Scale = new Vector3(0.03f, 0.03f, 0.03f);
            modelObject.Transform3D.Translation = new Vector3(70f, 5.7f, -42f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 225, 0));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Sofa";
            modelObject.Model = modelDictionary["hoh_sofa"];
            modelObject.EffectParameters.Texture = textureDictionary["initialShadingGroup_albedo"];
            modelObject.Transform3D.Scale = new Vector3(5.2f, 5.2f, 5.2f);
            modelObject.Transform3D.Translation = new Vector3(53f, 5.18f, -62f);
            modelObject.Transform3D.RotateBy(new Vector3(0, -45, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Rocking chair";
            modelObject.Model = modelDictionary["hoh_rocking chair"];
            modelObject.EffectParameters.Texture = textureDictionary["guitar_ba_lg_42"];
            modelObject.Transform3D.Scale = new Vector3(0.00565f, 0.00565f, 0.00565f);
            modelObject.Transform3D.Translation = new Vector3(70f, 5.18f, -91f);
            modelObject.Transform3D.RotateBy(new Vector3(90, 170, 0));
            modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Doll";
            modelObject.Model = modelDictionary["hoh_chucky"];
            modelObject.EffectParameters.Texture = textureDictionary["chuckyDiffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.75f, 0.71f, 0.75f);
            modelObject.Transform3D.Translation = new Vector3(68.7f, 5.22f, -90f);
            modelObject.Transform3D.RotateBy(new Vector3(-22, 75, 0));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Gramophone table";
            modelObject.Model = modelDictionary["hoh_gramophone table"];
            modelObject.EffectParameters.Texture = textureDictionary["wood-texture-background-timber-brown-yellow-old-wood-fibre-boards-pattern"];
            modelObject.Transform3D.Scale = new Vector3(0.18f, 0.18f, 0.18f);
            modelObject.Transform3D.Translation = new Vector3(27.2f, 2.56f, -75f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Gramophone";
            modelObject.Model = modelDictionary["hoh_gramophone"];
            modelObject.EffectParameters.Texture = textureDictionary["Gramophone"];
            modelObject.Transform3D.Scale = new Vector3(1.356f, 1.356f, 1.356f);
            modelObject.Transform3D.Translation = new Vector3(29f, 6.97f, -75f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, -70));
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bear trap";
            modelObject.Model = modelDictionary["hoh_bear trap"];
            modelObject.EffectParameters.Texture = textureDictionary["Trap1B"];
            modelObject.Transform3D.Scale = new Vector3(0.055f, 0.055f, 0.055f);
            modelObject.Transform3D.Translation = new Vector3(48.5f, 2.47f, -85f);
            modelObject.Transform3D.RotateBy(new Vector3(0, 0, 0));
            //modelObject = new TriangleMeshObject(modelObject.ID, ActorType.CollidableDecorator, StatusType.Update | StatusType.Drawn, modelObject.Transform3D, modelObject.EffectParameters, modelObject.Model, null,
            //    new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            #endregion ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            #region HALLWAY ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Door";
            modelObject.ActorType = ActorType.Win;
            modelObject.Model = modelDictionary["hoh_door"];
            modelObject.EffectParameters.Texture = textureDictionary["doorDiffuse"];
            modelObject.Transform3D.Scale = new Vector3(0.0182f, 0.0182f, 0.0182f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);
            modelObject.Transform3D.Translation = new Vector3(0f, 8f, -161.85f);
            modelObject.AddPrimitive(new Box(new Vector3(0f, 2.5f, -162.85f), Matrix.Identity, 2.54f * new Vector3(1, 1, 1)), new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Painting 1";
            modelObject.Model = modelDictionary["hoh_painting1"];
            modelObject.EffectParameters.Texture = textureDictionary["frame_horizontalDiffuseMap"];
            modelObject.Transform3D.Scale = new Vector3(0.018f, 0.018f, 0.018f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, -90, 0);
            modelObject.Transform3D.Translation = new Vector3(24.4f, 1.1f, -120f);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Painting 2";
            modelObject.Model = modelDictionary["hoh_painting2"];
            modelObject.EffectParameters.Texture = textureDictionary["frame_verticalDiffuseMap"];
            modelObject.Transform3D.Scale = new Vector3(0.018f, 0.018f, 0.018f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-24.2f, 6.5f, -120f);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Chandelier";
            modelObject.Model = modelDictionary["hoh_chandelier"];
            modelObject.EffectParameters.Texture = textureDictionary["vintage-bronze-copper-plate-non-ferrous-metal-sheet-as-backg_84485-37"];
            modelObject.Transform3D.Scale = new Vector3(0.018f, 0.018f, 0.018f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(0f, 7f, -104f);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Clock";
            modelObject.Model = modelDictionary["hoh_clock"];
            modelObject.EffectParameters.Texture = textureDictionary["grandfatherclock_uv"];
            modelObject.Transform3D.Scale = new Vector3(0.5f, 0.5f, 0.5f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-23.4f, 2f, -104f);
            modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hall Table";
            modelObject.Model = modelDictionary["hoh_hallTable"];
            modelObject.EffectParameters.Texture = textureDictionary["ASHSEN_2"];
            modelObject.Transform3D.Scale = new Vector3(0.0068f, 0.0068f, 0.0068f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, 90, 0);
            modelObject.Transform3D.Translation = new Vector3(-22.4f, 5f, -90.5f);
            modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Hall Table 2";
            modelObject.Model = modelDictionary["hoh_hallTable"];
            modelObject.EffectParameters.Texture = textureDictionary["ASHSEN_2"];
            modelObject.Transform3D.Scale = new Vector3(0.0068f, 0.0068f, 0.0068f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(90, -90, 0);
            modelObject.Transform3D.Translation = new Vector3(22.5f, 5f, -90.5f);
            modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            modelObject.Enable(true, 1);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Bust";
            modelObject.Model = modelDictionary["hoh_bust"];
            modelObject.Transform3D.Scale = new Vector3(0.02f, 0.02f, 0.02f);
            modelObject.Transform3D.RotationInDegrees = new Vector3(80, -94, 0);
            modelObject.Transform3D.Translation = new Vector3(23.8f, 6.3f, -90.5f);
            objectManager.Add(modelObject);

            modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            modelObject.ID = "Candlestick";
            modelObject.Model = modelDictionary["hoh_candlestick"];
            modelObject.EffectParameters.Texture = textureDictionary["Metal Seamless Texture #4116"];
            modelObject.Transform3D.Scale = new Vector3(0.0015f, 0.0015f, 0.0015f);
            modelObject.Transform3D.Translation = new Vector3(-22.8f, 5.8f, -90.34f);
            objectManager.Add(modelObject);

            #endregion ////////////////////////////////////////////////////////////////////////////////////////////////////////////          

            #region Keys
            //modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            //modelObject.ID = "Club key";
            //modelObject.Model = Content.Load<Model>("Assets/Models/Keys/Club Key");
            //modelObject.EffectParameters.Texture = Content.Load<Texture2D>("Assets/Textures/Keys/Metal_club");
            //modelObject.Transform3D.Scale = new Vector3(0.069f, 0.069f, 0.069f);
            //modelObject.Transform3D.RotationInDegrees = new Vector3(90, 90, 0);
            //modelObject.Transform3D.Translation = new Vector3(5f, 7f, -90f);
            //modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation*2, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            //objectManager.Add(modelObject);

            //modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            //modelObject.ID = "Heart Key";
            //modelObject.Model = Content.Load<Model>("Assets/Models/Keys/Heart Key");
            //modelObject.EffectParameters.Texture = Content.Load<Texture2D>("Assets/Textures/Keys/Metal_heart");
            //modelObject.Transform3D.Scale = new Vector3(0.013f, 0.013f, 0.013f);
            //modelObject.Transform3D.RotationInDegrees = new Vector3(0, 0, -90);
            //modelObject.Transform3D.Translation = new Vector3(-5f, 7f, -90f);
            //modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            //objectManager.Add(modelObject);

            //modelObject = this.archetypalBoxObject.Clone() as CollidableObject;
            //modelObject.ID = "Diamond key";
            //modelObject.Model = Content.Load<Model>("Assets/Models/Keys/Diamond Key");
            //modelObject.EffectParameters.Texture = Content.Load<Texture2D>("Assets/Textures/Keys/Metal_diamond");
            //modelObject.Transform3D.Scale = new Vector3(0.00078f, 0.00078f, 0.00078f);
            //modelObject.Transform3D.RotationInDegrees = new Vector3(0, 0, 0);
            //modelObject.Transform3D.Translation = new Vector3(0, 7f, -90f);
            //modelObject.AddPrimitive(new Box(modelObject.Transform3D.Translation, Matrix.Identity, 2.54f * modelObject.Transform3D.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));
            //modelObject.Enable(true, 1);
            //objectManager.Add(modelObject);

            #endregion

        }

        /// <summary>
        /// Initialises the three key objects.
        /// </summary>
        private void InitKeys()
        {
            PickupCollidableObject key;
            //PickupCollidableObject keyClone = null;
            Transform3D transform3D = null;
            EffectParameters effectParameters = null;

            effectParameters = new EffectParameters(modelEffect, textureDictionary["Metal_club"],
                Color.White, 1);

            transform3D = new Transform3D(spadeKeyLocation, -Vector3.UnitZ, Vector3.UnitY);

            //Spade key
            key = new PickupCollidableObject("Spade Key", ActorType.CollidableInventory,
                StatusType.Update | StatusType.Drawn, transform3D, effectParameters, modelDictionary["spadeKey"], -1);
            key.Transform3D.Scale = new Vector3(0.007f, 0.007f, 0.007f);

            key.AddPrimitive(new Box(key.Transform3D.Translation, Matrix.Identity,
                2.54f * new Vector3(.5f, .2f, .2f)), new MaterialProperties(0.2f, 0.8f, 0.7f));

            key.Enable(true, 1);
            objectManager.Add(key);

            //Heart key
            key = key.Clone() as PickupCollidableObject;
            key.ID = "Heart Key";
            key.Model = modelDictionary["heartKey"];
            key.EffectParameters.Texture = textureDictionary["Metal_heart"];
            key.Transform3D.Scale = new Vector3(0.001f, 0.001f, 0.001f);
            key.Transform3D.Translation = heartKeyLocation;
            key.Transform3D.RotationInDegrees = new Vector3(90, 0, 0);

            key.AddPrimitive(new Box(key.Transform3D.Translation, Matrix.Identity,
                2.54f * new Vector3(.5f, .2f, .2f)), new MaterialProperties(0.2f, 0.8f, 0.7f));

            key.Enable(true, 1);
            objectManager.Add(key);

            //Diamond Key
            key = key.Clone() as PickupCollidableObject;
            key.ID = "Diamond key";
            key.Model = modelDictionary["diamondKey"];
            key.EffectParameters.Texture = textureDictionary["Metal_diamond"];
            key.Transform3D.Scale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            key.Transform3D.Translation = diamondKeyLocation;
            key.Transform3D.RotationInDegrees = new Vector3(0, 0, 90);

            key.AddPrimitive(new Box(key.Transform3D.Translation, Matrix.Identity,
                2.54f * new Vector3(.5f, .2f, .2f)), new MaterialProperties(0.2f, 0.8f, 0.7f));

            key.Enable(true, 1);
            objectManager.Add(key);
        }

        /// <summary>
        /// Initialises the killer object and adds the controller for movement.
        /// </summary>
        private void InitKiller()
        {
            //transform
            Transform3D transform3D = new Transform3D(new Vector3(0, 0, -150f),
                                new Vector3(90, 0, -23),       //rotation
                                new Vector3(0.0004f, 0.0004f, 0.0004f),        //scale
                                    -Vector3.UnitZ,         //look
                                    Vector3.UnitY);         //up

            //effectparameters
            EffectParameters effectParameters = new EffectParameters(modelEffect,
                textureDictionary["killerDiffuse"],
                Color.White, 1);

            killerObject = new KillerObject("killer", ActorType.NonPlayer,
                StatusType.Drawn | StatusType.Update, transform3D,
                effectParameters,
                modelDictionary["killer"], menuManager);

            killerObject.AddPrimitive(new Box(killerObject.Transform3D.Translation, Matrix.Identity, 2.54f * new Vector3(3.5f, 3.5f, 3.5f)), new MaterialProperties(0.2f, 0.8f, 0.7f));
            killerObject.Enable(true, 1);

            //add the controller to move the killer
            killerObject.ControllerList.Add(
                new KillerController("killer path",
                ControllerType.Curve,
                        killerTransform3DCurveDictionary["killer route"], killerObject));

            objectManager.Add(killerObject);
        }

        /// <summary>
        /// Initialises any vertices to be drawn.
        /// </summary>
        private void InitVertices()
        {
            this.vertices
                = new VertexPositionColorTexture[4];

            float halfLength = 0.5f;

            //TL
            vertices[0] = new VertexPositionColorTexture(
                new Vector3(-halfLength, halfLength, 0),
                new Color(255, 255, 255, 255), new Vector2(0, 0));

            //TR
            vertices[1] = new VertexPositionColorTexture(
                new Vector3(halfLength, halfLength, 0),
                Color.White, new Vector2(1, 0));

            //BL
            vertices[2] = new VertexPositionColorTexture(
                new Vector3(-halfLength, -halfLength, 0),
                Color.White, new Vector2(0, 1));

            //BR
            vertices[3] = new VertexPositionColorTexture(
                new Vector3(halfLength, -halfLength, 0),
                Color.White, new Vector2(1, 1));
        }

        /// <summary>
        /// Initialises an archetypalTexturedQuad PrimitiveObject.
        /// </summary>
        private void InitPrimitiveArchetypes() //formerly InitTexturedQuad
        {
            Transform3D transform3D = new Transform3D(Vector3.Zero, Vector3.Zero,
               Vector3.One, Vector3.UnitZ, Vector3.UnitY);

            EffectParameters effectParameters = new EffectParameters(unlitTexturedEffect,
                grass, /*bug*/ Color.White, 1);

            IVertexData vertexData = new VertexData<VertexPositionColorTexture>(
                vertices, Microsoft.Xna.Framework.Graphics.PrimitiveType.TriangleStrip, 2);

            archetypalTexturedQuad = new PrimitiveObject("original texture quad",
                ActorType.Decorator,
                StatusType.Update | StatusType.Drawn,
                transform3D, effectParameters, vertexData);
        }

        //VertexPositionColorTexture - 4 bytes x 3 (x,y,z) + 4 bytes x 3 (r,g,b) + 4bytes x 2 = 26 bytes
        //VertexPositionColor -  4 bytes x 3 (x,y,z) + 4 bytes x 3 (r,g,b) = 24 bytes

        /// <summary>
        /// Adds a primitive object to the object manager.
        /// </summary>
        private void InitHelpers()
        {
            //to do...add wireframe origin
            Microsoft.Xna.Framework.Graphics.PrimitiveType primitiveType;
            int primitiveCount;

            //step 1 - vertices
            VertexPositionColor[] vertices = VertexFactory.GetVerticesPositionColorOriginHelper(
                                    out primitiveType, out primitiveCount);

            //step 2 - make vertex data that provides Draw()
            IVertexData vertexData = new VertexData<VertexPositionColor>(vertices,
                                    primitiveType, primitiveCount);

            //step 3 - make the primitive object
            Transform3D transform3D = new Transform3D(new Vector3(0, 20, 0),
                Vector3.Zero, new Vector3(10, 10, 10),
                Vector3.UnitZ, Vector3.UnitY);

            EffectParameters effectParameters = new EffectParameters(unlitWireframeEffect,
                null, Color.White, 1);

            //at this point, we're ready!
            PrimitiveObject primitiveObject = new PrimitiveObject("origin helper",
                ActorType.Helper, StatusType.Drawn, transform3D, effectParameters, vertexData);

            objectManager.Add(primitiveObject);

        }

        /// <summary>
        /// Initialises the skybox in the level.
        /// </summary>
        private void InitSkybox()
        {
            //back
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            //  primitiveObject.StatusType = StatusType.Off; //Experiment of the effect of StatusType
            primitiveObject.ID = "sky back";
            primitiveObject.EffectParameters.Texture = this.backSky;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            primitiveObject.Transform3D.Translation = new Vector3(0, 800, -worldScale / 2.0f);
            this.objectManager.Add(primitiveObject);

            //left
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            primitiveObject.ID = "left back";
            primitiveObject.EffectParameters.Texture = this.leftSky;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            primitiveObject.Transform3D.RotationInDegrees = new Vector3(0, 90, 0);
            primitiveObject.Transform3D.Translation = new Vector3(-worldScale / 2.0f, 800, 0);
            this.objectManager.Add(primitiveObject);

            //right
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            primitiveObject.ID = "sky right";
            primitiveObject.EffectParameters.Texture = this.rightSky;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 20);
            primitiveObject.Transform3D.RotationInDegrees = new Vector3(0, -90, 0);
            primitiveObject.Transform3D.Translation = new Vector3(worldScale / 2.0f, 800, 0);
            this.objectManager.Add(primitiveObject);


            //top
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            primitiveObject.ID = "sky top";
            primitiveObject.EffectParameters.Texture = this.topSky;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            primitiveObject.Transform3D.RotationInDegrees = new Vector3(90, -90, 0);
            primitiveObject.Transform3D.Translation = new Vector3(0, worldScale / 2.0f, 0);
            this.objectManager.Add(primitiveObject);

            //to do...front
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            primitiveObject.ID = "sky front";
            primitiveObject.EffectParameters.Texture = this.frontSky;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            primitiveObject.Transform3D.RotationInDegrees = new Vector3(0, 180, 0);
            primitiveObject.Transform3D.Translation = new Vector3(0, 800, worldScale / 2.0f);
            this.objectManager.Add(primitiveObject);

        }

        /// <summary>
        /// Initialises the ground in the level.
        /// </summary>
        private void InitGround()
        {
            //grass
            primitiveObject = this.archetypalTexturedQuad.Clone() as PrimitiveObject;
            primitiveObject.ID = "ground";
            primitiveObject.EffectParameters.Texture = this.ground;
            primitiveObject.Transform3D.Scale = new Vector3(worldScale, worldScale, 1);
            primitiveObject.Transform3D.RotationInDegrees = new Vector3(90, 90, 0);
            this.objectManager.Add(primitiveObject);
        }

        /// <summary>
        /// Initialises the graphics (sets resolution, etc.).
        /// </summary>
        private void InitGraphics(int width, int height)
        {
            //set resolution
            this._graphics.PreferredBackBufferWidth = width;
            this._graphics.PreferredBackBufferHeight = height;

            //dont forget to apply resolution changes otherwise we wont see the new WxH
            this._graphics.ApplyChanges();

            //set screen centre based on resolution
            this.screenCentre = new Vector2(width / 2, height / 2);

            //set cull mode to show front and back faces - inefficient but we will change later
            RasterizerState rs = new RasterizerState
            {
                CullMode = CullMode.None
            };
            this._graphics.GraphicsDevice.RasterizerState = rs;

            //we use a sampler state to set the texture address mode to solve the aliasing problem between skybox planes
            SamplerState samplerState = new SamplerState
            {
                AddressU = TextureAddressMode.Clamp,
                AddressV = TextureAddressMode.Clamp
            };
            this._graphics.GraphicsDevice.SamplerStates[0] = samplerState;
        }

        /// <summary>
        /// Loads any effects that will be applied to objects.
        /// </summary>
        private void LoadEffects()
        {
            unlitTexturedEffect = new BasicEffect(_graphics.GraphicsDevice);
            unlitTexturedEffect.VertexColorEnabled = true; //otherwise we wont see RGB
            unlitTexturedEffect.TextureEnabled = true;

            unlitTexturedEffect.FogEnabled = true;
            unlitTexturedEffect.FogColor = Color.TransparentBlack.ToVector3();
            unlitTexturedEffect.FogStart = 0f;
            unlitTexturedEffect.FogEnd = 40f;

            //wireframe primitives with no lighting and no texture
            unlitWireframeEffect = new BasicEffect(_graphics.GraphicsDevice);
            unlitWireframeEffect.VertexColorEnabled = true;

            //model effect
            modelEffect = new BasicEffect(_graphics.GraphicsDevice);
            modelEffect.TextureEnabled = true;
            modelEffect.LightingEnabled = true;
            modelEffect.PreferPerPixelLighting = true;

            modelEffect.FogEnabled = true;
            modelEffect.FogColor = Color.TransparentBlack.ToVector3();
            modelEffect.FogStart = 0f;
            modelEffect.FogEnd = 40f;

            //   this.modelEffect.SpecularPower = 512;
            //  this.modelEffect.SpecularColor = Color.Red.ToVector3();
            modelEffect.EnableDefaultLighting();
        }

        /// <summary>
        /// Loads all textures and adds them to a texture dictionary.
        /// </summary>
        private void LoadTextures()
        {
            //sky
            textureDictionary.Load("Assets/Textures/Skybox/back");
            textureDictionary.Load("Assets/Textures/Skybox/left");
            textureDictionary.Load("Assets/Textures/Skybox/right");
            textureDictionary.Load("Assets/Textures/Skybox/front");
            textureDictionary.Load("Assets/Textures/Skybox/sky");
            textureDictionary.Load("Assets/Textures/Foliage/Ground/grass1");

            //house items
            textureDictionary.Load("Assets/Textures/HouseItems/0");
            textureDictionary.Load("Assets/Textures/HouseItems/0088-dark-raw-wood-texture-seamless-hr");
            textureDictionary.Load("Assets/Textures/HouseItems/ASHSEN_2");
            textureDictionary.Load("Assets/Textures/HouseItems/bedDiffuse");
            textureDictionary.Load("Assets/Textures/HouseItems/Bricks Seamless Texture #3415");
            textureDictionary.Load("Assets/Textures/HouseItems/cabinet_Cabinet_BaseColor");
            textureDictionary.Load("Assets/Textures/HouseItems/Chair");
            textureDictionary.Load("Assets/Textures/HouseItems/chuckyDiffuse");
            textureDictionary.Load("Assets/Textures/HouseItems/diffuse");
            textureDictionary.Load("Assets/Textures/HouseItems/frame_horizontalDiffuseMap");
            textureDictionary.Load("Assets/Textures/HouseItems/frame_verticalDiffuseMap");
            textureDictionary.Load("Assets/Textures/HouseItems/fridgeDiffuse");
            textureDictionary.Load("Assets/Textures/HouseItems/Gramophone");
            textureDictionary.Load("Assets/Textures/HouseItems/grandfatherclock_uv");
            textureDictionary.Load("Assets/Textures/HouseItems/guitar_ba_lg_42");
            textureDictionary.Load("Assets/Textures/HouseItems/initialShadingGroup_albedo");
            textureDictionary.Load("Assets/Textures/HouseItems/kitchen_main_stand_light_map");
            textureDictionary.Load("Assets/Textures/HouseItems/Knife_Diffuse");
            textureDictionary.Load("Assets/Textures/HouseItems/Metal Seamless Texture #4116");
            textureDictionary.Load("Assets/Textures/HouseItems/Old Wooden Plank Seamless Texture #477");
            textureDictionary.Load("Assets/Textures/HouseItems/SHELF_TEXTURE");
            textureDictionary.Load("Assets/Textures/HouseItems/Smooth wood seamless Texture #869");
            textureDictionary.Load("Assets/Textures/HouseItems/TV_Color");
            textureDictionary.Load("Assets/Textures/HouseItems/07_-_Default_Base_Color");
            textureDictionary.Load("Assets/Textures/HouseItems/vintage-bronze-copper-plate-non-ferrous-metal-sheet-as-backg_84485-37");
            textureDictionary.Load("Assets/Textures/HouseItems/Wood028_4K_Color");
            textureDictionary.Load("Assets/Textures/HouseItems/wood-texture-background-timber-brown-yellow-old-wood-fibre-boards-pattern");
            textureDictionary.Load("Assets/Textures/HouseItems/doorDiffuse");

            //keys
            textureDictionary.Load("Assets/Textures/Keys/Metal_club");
            textureDictionary.Load("Assets/Textures/Keys/Metal_diamond");
            textureDictionary.Load("Assets/Textures/Keys/Metal_heart");

            //traps
            textureDictionary.Load("Assets/Textures/Trap/Trap1B");

            //killer
            textureDictionary.Load("Assets/Textures/Killer/killerDiffuse");

            //ui
            textureDictionary.Load("Assets/Textures/Player/Crosshairs/crossHair");
            textureDictionary.Load("Assets/Textures/UI/Controls/HOH_keyHudFrame");
            textureDictionary.Load("Assets/Textures/UI/Controls/diamondSprite");
            textureDictionary.Load("Assets/Textures/UI/Controls/heartSprite");
            textureDictionary.Load("Assets/Textures/UI/Controls/spadeSprite");

            //menu
            textureDictionary.Load("Assets/Textures/UI/Controls/genericbtn");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/mainmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/audiomenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/controlsmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/credits");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/controls");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/winScene");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/loseScene");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/exitmenu");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/exitmenuwithtrans");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/menuBG");
            textureDictionary.Load("Assets/Textures/UI/Backgrounds/howToPlay");

        }

        /// <summary>
        /// Loads all models and adds them to a model dictionary.
        /// </summary>
        private void LoadModels()
        {
            //box
            modelDictionary.Load("Assets/Models/box2");

            //house items
            modelDictionary.Load("Assets/Models/HouseItems/hoh_bed");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_bedside table");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_bigWindow");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_bookshelf");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_bust");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_cabinet");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_candlestick");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_chandelier");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_chucky");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_clock");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_counter");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_desk");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_dining table");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_diningChair");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_fridge");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_gramophone table");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_gramophone");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_hallTable");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_kitchen counters");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_kitchen table");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_knife");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_painting1");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_painting2");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_rocking chair");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_sofa");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_teapot");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_Tv");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_tvTable");
            modelDictionary.Load("Assets/Models/HouseItems/hoh_door");

            //trap
            modelDictionary.Load("Assets/Models/Trap/hoh_bear trap");

            //killer
            modelDictionary.Load("Assets/Models/Killer/killer");

            //keys
            modelDictionary.Load("Assets/Models/Keys/spadeKey");
            modelDictionary.Load("Assets/Models/Keys/diamondKey");
            modelDictionary.Load("Assets/Models/Keys/heartKey");
        }

        /// <summary>
        /// Initialises the ui (not yet implemented).
        /// </summary>
        private void InitUI()
        {
            Transform2D transform2D = null;
            Texture2D texture = null;
            SpriteFont spriteFont = null;

            // KEY HUD FRAME
            texture = textureDictionary["HOH_keyHudFrame"];

            transform2D = new Transform2D(new Vector2(100, 50),
                    0,
                    Vector2.One,
                    new Vector2(texture.Width / 2, texture.Height / 2),
                    new Integer2(0, 0));

            UITextureObject uiTextureObject = new UITextureObject("keyHudFrame", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.White, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            uiManager.Add(uiTextureObject);

            // DIAMOND HUD SPRITE
            texture = textureDictionary["diamondSprite"];

            transform2D = new Transform2D(new Vector2(45, 50),
                    0,
                    Vector2.One,
                    new Vector2(texture.Width / 2, texture.Height / 2),
                    new Integer2(0, 0));

            diamondHudIcon = new UITextureObject("diamondSprite", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.White, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            diamondHudIcon.StatusType = StatusType.Off;
            uiManager.Add(diamondHudIcon);

            // HEART HUD SPRITE
            texture = textureDictionary["heartSprite"];

            transform2D = new Transform2D(new Vector2(100, 50),
                    0,
                    Vector2.One,
                    new Vector2(texture.Width / 2, texture.Height / 2),
                    new Integer2(0, 0));

            heartHudIcon = new UITextureObject("heartSprite", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.White, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            heartHudIcon.StatusType = StatusType.Off;
            uiManager.Add(heartHudIcon);

            // SPADE HUD SPRITE
            texture = textureDictionary["spadeSprite"];

            transform2D = new Transform2D(new Vector2(155, 50),
                    0,
                    Vector2.One,
                    new Vector2(texture.Width / 2, texture.Height / 2),
                    new Integer2(0, 0));

            spadeHudIcon = new UITextureObject("spadeSprite", ActorType.UITextureObject,
                StatusType.Drawn | StatusType.Update, transform2D, Color.White, 0, SpriteEffects.None,
                texture,
                new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            spadeHudIcon.StatusType = StatusType.Off;
            uiManager.Add(spadeHudIcon);

            #region Progress Control Left
            //texture = textureDictionary["progress_white"];

            //transform2D = new Transform2D(new Vector2(512, 250),
            //    -90,
            //     Vector2.One,
            //    new Vector2(texture.Width / 2, texture.Height / 2),
            //    new Integer2(100, 100));

            //UITextureObject uiTextureObject = new UITextureObject("progress 1", ActorType.UITextureObject,
            //    StatusType.Drawn | StatusType.Update, transform2D, Color.Yellow, 0, SpriteEffects.None,
            //    texture,
            //    new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height));

            //uiTextureObject.ControllerList.Add(new UIRotationController("rc1", ControllerType.RotationOverTime));

            //uiTextureObject.ControllerList.Add(new UIColorLerpController("clc1", ControllerType.ColorLerpOverTime,
            //    Color.White, Color.Black));

            //uiTextureObject.ControllerList.Add(new UIMouseController("moc1", ControllerType.MouseOver,
            //    this.mouseManager));

            //uiTextureObject.ControllerList.Add(new UIProgressController("pc1", ControllerType.Progress, 0, 10));

            //uiManager.Add(uiTextureObject);
            #endregion Progress Control Left

            #region Text Object
            spriteFont = fontDictionary["menu"];

            //calculate how big the text is in (w, h)
            string text = "( KEYS )";
            Vector2 originalDimensions = spriteFont.MeasureString(text);

            transform2D = new Transform2D(new Vector2(100, 15),
                0,
                Vector2.One / 3.5f,
                new Vector2(originalDimensions.X / 2, originalDimensions.Y / 2), //this is text???
                new Integer2(originalDimensions)); //accurate original dimensions

            UITextObject uiTextObject = new UITextObject("keyTitle", ActorType.UIText,
                StatusType.Update | StatusType.Drawn, transform2D, new Color(0.1f, 1, 1, 1),
                0, SpriteEffects.None, text, spriteFont);

            //uiTextObject.ControllerList.Add(new UIMouseController("moc1", ControllerType.MouseOver,
            //     mouseManager));

            uiManager.Add(uiTextObject);
            #endregion Text Object
        }

        //protected override void LoadContent()
        //{w
        //    _spriteBatch = new SpriteBatch(GraphicsDevice);

        //}

        /// <summary>
        /// Unloads all content in the dictionaries.
        /// </summary>
        protected override void UnloadContent()
        {
            textureDictionary.Dispose();
            modelDictionary.Dispose();
            fontDictionary.Dispose();
            modelDictionary.Dispose();

            base.UnloadContent();
        }
        #endregion

        #region Update & Draw
        protected override void Update(GameTime gameTime)
        {
            CollidableFirstPersonCameraController cfpcc;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || this.keyboardManager.IsFirstKeyPress(Keys.Escape))
                Exit();

            if (this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                this.cameraManager.CycleActiveCamera();
                EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.OnCameraCycle, null));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.T))
            {
                EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.OnShowDebugCameras, null));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Y))
            {
                EventDispatcher.Publish(new EventData(EventCategoryType.Camera, EventActionType.OnShowGameCamera, null));
            }



            if (this.keyboardManager.IsFirstKeyPress(Keys.F1))
            {
                InitDebug();
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.F2))
            {
                RemoveDebug();
            }


            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);



            base.Draw(gameTime);

            _spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, DepthStencilState.Default, null);
            _spriteBatch.Draw(textureDictionary["crossHair"], new Microsoft.Xna.Framework.Rectangle(470, 350, 75, 75), Color.White);
            _spriteBatch.End();

        }

        #endregion
    }
}