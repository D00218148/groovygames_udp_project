﻿using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Parameters;
using GDLibrary.Events;
using JigLibX.Collision;
using Microsoft.Xna.Framework.Graphics;
using GDLibrary.Managers;

namespace GDGame.Actors
{
    public class KillerObject : CollidableObject
    {
        bool playerFound = false;
        MenuManager menuManager;
        int hitCount = 0;

        public KillerObject(string id, ActorType actorType, StatusType statusType,
            Transform3D transform, EffectParameters effectParameters, Model model, MenuManager menuManager)
            : base(id, actorType, statusType, transform, effectParameters, model)
        {       
            //step 1 - add code to listen for CDCR event
            this.Body.CollisionSkin.callbackFn += HandleCollision;
            this.menuManager = menuManager;
        }

        //step 2 - add a handler
        private bool HandleCollision(CollisionSkin collider, CollisionSkin collidee)
        {
            //step 3 - cast the collidee (the object you hit) to access its fields
            CollidableObject collidableObject = collidee.Owner.ExternalData as CollidableObject;

            //step 4 - make a decision on what you're going to do based on, say, the ActorType
            if (collidableObject.ActorType == ActorType.CollidableCamera)
            {
                if (playerFound == false)
                {
                    object[] parameters = { "contactwithkiller" };
                    EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));
       
                    playerFound = true;
                }

                hitCount++;

                if (hitCount == 1)
                {
                    System.Diagnostics.Debug.WriteLine(hitCount + " ****************************************************************************************************************");
                    //show menu
                    EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));

                    menuManager.SetScene("loseScene");

                    hitCount = 0;
                }
            }

            return true;
        }

        public new object Clone()
        {
            return new KillerObject("clone - " + ID, //deep
                ActorType,   //deep
                StatusType,
                Transform3D.Clone() as Transform3D,  //deep
                EffectParameters.Clone() as EffectParameters, //hybrid - shallow (texture and effect) and deep (all other fields)
                Model, //shallow i.e. a reference
                menuManager); 
        }
    }
}