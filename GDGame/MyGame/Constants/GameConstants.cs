﻿using Microsoft.Xna.Framework.Input;

namespace GDGame
{
    public class GameConstants
    {
        /*
        #region Common

        private static readonly float strafeSpeedMultiplier = 0.75f;
        public static readonly Keys[] KeysOne = { Keys.W, Keys.S, Keys.A, Keys.D };
        public static readonly Keys[] KeysTwo = { Keys.U, Keys.J, Keys.H, Keys.K };

        #endregion Common

        #region First Person Camera

        public static readonly float moveSpeed = 0.1f;
        public static readonly float strafeSpeed = strafeSpeedMultiplier * moveSpeed;
        public static readonly float rotateSpeed = 0.01f;

        #endregion First Person Camera

        #region Flight Camera

        public static readonly float flightMoveSpeed = 0.8f;
        public static readonly float flightStrafeSpeed = strafeSpeedMultiplier * flightMoveSpeed;
        public static readonly float flightRotateSpeed = 0.01f;

        #endregion Flight Camera

        #region Security Camera

        private static readonly float angularSpeedMultiplier = 10;
        public static readonly float lowAngularSpeed = 10;
        public static readonly float mediumAngularSpeed = lowAngularSpeed * angularSpeedMultiplier;
        public static readonly float hiAngularSpeed = mediumAngularSpeed * angularSpeedMultiplier;

        #endregion Security Camera
        */
        #region Camera
        private static readonly float angularSpeedMultiplier = 10;
        public static readonly float lowAngularSpeed = 10;
        public static readonly float mediumAngularSpeed = lowAngularSpeed * angularSpeedMultiplier;
        public static readonly float hiAngularSpeed = mediumAngularSpeed * angularSpeedMultiplier;
        #endregion

        #region Collidable First Person Camera

        public static readonly Keys[] CameraMoveKeys = {Keys.W, Keys.S, Keys.A, Keys.D, 
                                        Keys.LeftShift, //Run
                                        Keys.LeftControl, //Crouch
                                        Keys.Space, //Jump
                                        Keys.Up, Keys.Down, Keys.Left, Keys.Right,
                                        Keys.RightShift, //Run
                                        Keys.RightControl, //Crouch
                                        Keys.RightAlt}; //Toggle which crouch button to use, right or left.

        //JigLib related collidable camera properties
        public static readonly float CollidableCameraJumpHeight = 14;
        public static readonly float CollidableCameraMoveSpeed = 0.125f;
        public static readonly float CollidableCameraStrafeSpeed = 0.6f * CollidableCameraMoveSpeed;
        public static readonly float CollidableCameraCapsuleRadius = 2;
        public static readonly float CollidableCameraViewHeight = 8; //how tall is the first person player?
        public static readonly float CollidableCameraMass = 10;

        #endregion Collidable First Person Camera

        #region Player

        private static readonly float strafeSpeedMultiplier = 0.75f;
        public static readonly float moveSpeed = 0.005f;
        public static readonly float debugMoveSpeed = 0.1f;
        public static readonly float scrollSpeed = 100f;
        public static readonly float runMultiplier = 1.4f;
        public static readonly float strafeSpeed = strafeSpeedMultiplier * moveSpeed;
        public static readonly float debugStrafeSpeed = 0.01f;
        public static readonly float rotateSpeed = 0.01f;
        //keys
        public static readonly Keys[] LeftKeys =
            {
                Keys.W, Keys.S, Keys.A, Keys.D, Keys.LeftShift, Keys.LeftControl
            };
        public static readonly Keys[] RightKeys =
            {
                Keys.Up, Keys.Down, Keys.Left, Keys.Right, Keys.RightShift, Keys.RightControl, Keys.RightAlt
            };
        #endregion

        #region Killer

        public static readonly float killerMoveSpeed = 0.005f;
        public static readonly float killerRotateSpeed = 0.01f;
        #endregion
    }
}