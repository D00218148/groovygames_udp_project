﻿using GDLibrary.Enums;
using GDLibrary.Events;
using GDLibrary.GameComponents;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GDGame
{
    /// <summary>
    /// Manages what happens in the game when the player loses or wins the game
    /// </summary>
    public class GameStateManager : PausableGameComponent
    {
        private int keysCollected;
        private MenuManager menuManager;

        public int KeysCollected
        {
            get
            {
                return keysCollected;
            }
            set
            {
                keysCollected = value;
            }
        }

        public GameStateManager(Game game, StatusType statusType, MenuManager menuManager) : base(game, statusType)
        {
            this.KeysCollected = 0;
            this.menuManager = menuManager;
        }

        protected override void SubscribeToEvents()
        {
            EventDispatcher.Subscribe(EventCategoryType.WinLose, HandleEvent);

            base.SubscribeToEvents();
        }

        protected override void HandleEvent(EventData eventData)
        {
            if(eventData.EventActionType == EventActionType.OnWin)
            {
                WinGame();
            }
            else if(eventData.EventActionType == EventActionType.OnLose)
            {
                LoseGame();
            }
            else if(eventData.EventActionType == EventActionType.OnPickup)
            {
                keysCollected += 1;
            }
            else if(eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPlay)
            {
                object[] parameters = { "gametheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

                object[] parameters2 = { "menutheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters2));
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnStop)
            {
                
                object[] parameters = { "gametheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));

                object[] parameters2 = { "menutheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
            }
            else if (eventData.EventCategoryType == EventCategoryType.Menu && eventData.EventActionType == EventActionType.OnPause)
            {
                object[] parameters = { "gametheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnStop, parameters));

                object[] parameters2 = { "menutheme" };
                EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters2));
            }


            base.HandleEvent(eventData);
        }

        public void WinGame()
        {
            //Game.Exit();

            object[] parameters = { "wingrandpiano" };
            EventDispatcher.Publish(new EventData(EventCategoryType.Sound, EventActionType.OnPlay2D, parameters));

            //show menu
            EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));

            //Set the scene
            this.menuManager.SetScene("winScene");
        }

        public void LoseGame()
        {
            //Game.Exit();

            //show menu
            EventDispatcher.Publish(new EventData(EventCategoryType.Menu, EventActionType.OnPause, null));

            //Set the scene
            this.menuManager.SetScene("winScene");
        }
    }
}
