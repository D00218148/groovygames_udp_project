﻿using GDGame;
using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;

namespace GDLibrary
{
    public class DebugFirstPersonCameraController : IController
    {
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private float debugMoveSpeed, scrollSpeed, debugStrafeSpeed, rotationSpeed;
        private bool setLook = false;

        public DebugFirstPersonCameraController(KeyboardManager keyboardManager,
            MouseManager mouseManager,
            float debugMoveSpeed, float scrollSpeed,
            float debugStrafeSpeed, float rotationSpeed)
        {
            this.keyboardManager = keyboardManager;
            this.mouseManager = mouseManager;
            this.debugMoveSpeed = debugMoveSpeed;
            this.debugStrafeSpeed = debugStrafeSpeed;
            this.rotationSpeed = rotationSpeed;
            this.scrollSpeed = scrollSpeed;
        }

        public void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parent = actor as Actor3D;

            if (parent != null)
            {
                HandleKeyboardInput(gameTime, parent);
                HandleMouseRotation(gameTime, parent);
                HandleMouseZoom(gameTime, parent);
            }
        }

        private void HandleKeyboardInput(GameTime gameTime, Actor3D parent)
        {
            Vector3 moveVector = Vector3.Zero;

            #region UP AND DOWN MOVEMENT ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[0]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[0]))
            {
                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
                {
                    // RUN
                    moveVector = parent.Transform3D.Look * (this.debugMoveSpeed * GameConstants.runMultiplier);
                }
                else
                {
                    // WALK
                    moveVector = parent.Transform3D.Look * this.debugMoveSpeed;
                }
            }
            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[1]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[1]))
            {
                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
                {
                    // RUN BACKWARDS (slower than run forwards)
                    moveVector = -1 * parent.Transform3D.Look * (this.debugMoveSpeed * (GameConstants.runMultiplier - 0.75f));
                }
                else
                {
                    // WALK BACKWARDS
                    moveVector = -1 * parent.Transform3D.Look * this.debugMoveSpeed;
                }
            }
            #endregion//////////////////////////////////////////////////////

            #region STRAFFING //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[2]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[2]))
            {
                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
                {
                    // STRAFE FASTER LEFT
                    moveVector -= parent.Transform3D.Right * (this.debugStrafeSpeed) / 1000;
                }
                else
                {
                    // STRAFE LEFT
                    moveVector -= parent.Transform3D.Right * this.debugStrafeSpeed / 1000;
                }
            }
            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[3]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[3]))
            {
                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
                {
                    // STRAFE FASTER RIGHT  
                    moveVector += parent.Transform3D.Right * (this.debugStrafeSpeed) / 1000;
                }
                else
                {
                    // STRAFE RIGHT
                    moveVector += parent.Transform3D.Right * this.debugStrafeSpeed / 1000;
                }
            }
            #endregion ///////

            // Constrain movement in Y-axis
            moveVector.Y = 0;

            // Move everything with respect to deltaTime
            parent.Transform3D.TranslateBy(moveVector * gameTime.ElapsedGameTime.Milliseconds);
        }

        private void HandleMouseRotation(GameTime gameTime, Actor3D parent)
        {
            this.mouseManager.MouseVisible = false;

            // Set the mouse position to the center of window once at start
            while (setLook == false)
            {
                this.mouseManager.SetPosition(new Vector2(512, 384));
                setLook = true;
            }

            Vector2 mouseDelta = this.mouseManager.GetDeltaFromCentre(new Vector2(512, 384));
            mouseDelta *= this.rotationSpeed * gameTime.ElapsedGameTime.Milliseconds;

            if (mouseDelta.Length() != 0)
            {
                if (this.mouseManager.HasMovedRight(0.01f))
                {
                    if (this.mouseManager.Position.X > 1022.9f)
                    {
                        float mousePosY = this.mouseManager.Position.Y;

                        this.mouseManager.SetPosition(new Vector2(1, mousePosY));
                    }
                }
                else if (this.mouseManager.HasMovedLeft(0.01f))
                {
                    if (this.mouseManager.Position.X < 1)
                    {
                        float mousePosY = this.mouseManager.Position.Y;

                        this.mouseManager.SetPosition(new Vector2(1022.9f, mousePosY));
                    }
                }

                parent.Transform3D.RotateBy(new Vector3(-2.2f * mouseDelta.X, -1 * mouseDelta.Y, 0));
            }
        }

        private void HandleMouseZoom(GameTime gameTime, Actor3D parent)
        {
            Vector3 moveVector = Vector3.Zero;
            int mWheelDelta = this.mouseManager.GetDeltaFromScrollWheel();           
  
            if (mWheelDelta > 0)
            {                   
                moveVector = parent.Transform3D.Look; 
            }
            else if (mWheelDelta < 0)
            {                
                moveVector = parent.Transform3D.Look * -1; 
            }

            //System.Diagnostics.Debug.WriteLine("Mouse Wheel Delta: " + mWheelDelta + "**********************************\n");
            parent.Transform3D.TranslateBy(moveVector * gameTime.ElapsedGameTime.Milliseconds * scrollSpeed * 10);
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public ControllerType GetControllerType()
        {
            throw new NotImplementedException();
        }
    }
}
