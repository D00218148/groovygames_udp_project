﻿using GDLibrary.Actors;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters;
using Microsoft.Xna.Framework;

namespace GDLibrary.Controllers
{
    /// <summary>
    /// Moves a target actor along a pre-defined curve defined by transformCurve3D
    /// </summary>
    public class KillerController : Controller
    {
        #region Statics
        private static int EVALUATE_PRECISION = 3;
        #endregion Statics

        #region Fields
        private KillerTransform3DCurve killerTransform3DCurve;
        private CollidableObject killerObject;
        private int elapsedTimeInMs = 0;
        #endregion Fields

        #region Constructors & Core
        public KillerController(string id, ControllerType controllerType,
         KillerTransform3DCurve transform3DCurve, CollidableObject killerObject) : base(id, controllerType)
        {
            this.killerTransform3DCurve = transform3DCurve;
            this.killerObject = killerObject;
        }

        //local
        Vector3 translation, rotation, up;
        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parent = actor as Actor3D;

            if (parent != null)
            {
                elapsedTimeInMs += gameTime.ElapsedGameTime.Milliseconds;
                killerTransform3DCurve.Evalulate(elapsedTimeInMs, EVALUATE_PRECISION, out translation, out rotation, out up);

                killerObject.Body.MoveTo(translation, Matrix.Identity);
                parent.Transform3D.Translation = translation;
                parent.Transform3D.RotationInDegrees = rotation;
                parent.Transform3D.Up = up;
            }
        }
        #endregion Constructors & Core
    }
}