﻿//using GDGame;
//using GDLibrary.Actors;
//using GDLibrary.Enums;
//using GDLibrary.Interfaces;
//using GDLibrary.Managers;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Input;
//using System;
//using System.Diagnostics;

//namespace GDLibrary
//{
//    public class FirstPersonCameraController : IController
//    {
//        private KeyboardManager keyboardManager;
//        private MouseManager mouseManager;
//        private float moveSpeed, strafeSpeed, rotationSpeed;
//        private bool setLook = false;
//        private bool isCrouching = false;
//        private bool rightCtrlEnabled = false; // Change to true if you want to use the Right Ctrl Key
//        private bool rightAltHeld = false;

//        public FirstPersonCameraController(KeyboardManager keyboardManager,
//            MouseManager mouseManager,
//            float moveSpeed,
//            float strafeSpeed, float rotationSpeed)
//        {
//            this.keyboardManager = keyboardManager;
//            this.mouseManager = mouseManager;
//            this.moveSpeed = moveSpeed;
//            this.strafeSpeed = strafeSpeed;
//            this.rotationSpeed = rotationSpeed;
//        }

//        public void Update(GameTime gameTime, IActor actor)
//        {
//            Actor3D parent = actor as Actor3D;

//            if(parent != null)
//            {
//                HandleKeyboardInput(gameTime, parent);
//                HandleMouseInput(gameTime, parent);
//            }
//        }

//        private void HandleKeyboardInput(GameTime gameTime, Actor3D parent)
//        {
//            Vector3 moveVector = Vector3.Zero;

//            #region UP AND DOWN MOVEMENT ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[0]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[0]))
//            {
//                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
//                {
//                    // RUN
//                    moveVector = parent.Transform3D.Look * (this.moveSpeed * GameConstants.runMultiplier);
//                }
//                else
//                {
//                    // WALK
//                    moveVector = parent.Transform3D.Look * this.moveSpeed;
//                }
//            }
//            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[1]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[1]))
//            {
//                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
//                {
//                    // RUN BACKWARDS (slower than run forwards)
//                    moveVector = -1 * parent.Transform3D.Look * (this.moveSpeed * (GameConstants.runMultiplier - 0.75f));
//                }
//                else
//                {
//                    // WALK BACKWARDS
//                    moveVector = -1 * parent.Transform3D.Look * this.moveSpeed;
//                }
//            }
//            #endregion//////////////////////////////////////////////////////

//            #region STRAFFING //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[2]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[2]))
//            {
//                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
//                {
//                    // STRAFE FASTER LEFT
//                    moveVector -= parent.Transform3D.Right * (this.strafeSpeed * (GameConstants.runMultiplier - 0.75f));
//                }
//                else
//                {
//                    // STRAFE LEFT
//                    moveVector -= parent.Transform3D.Right * this.strafeSpeed;
//                }
//            }
//            if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[3]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[3]))
//            {
//                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[4]) || this.keyboardManager.IsKeyDown(GameConstants.RightKeys[4]))
//                {
//                    // STRAFE FASTER RIGHT  
//                    moveVector += parent.Transform3D.Right * (this.strafeSpeed * (GameConstants.runMultiplier - 0.75f));
//                }
//                else
//                {
//                    // STRAFE RIGHT
//                    moveVector += parent.Transform3D.Right * this.strafeSpeed;
//                }
//            }
//            #endregion ///////

//            //Debug.WriteLine("\nTranslation.Y: " + parent.Transform3D.Translation.Y + "\n");
//            //Debug.WriteLine("\nisCrouching: " + isCrouching + "\n");

//            #region CROUCHING //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            // Press Right Alt key to toggle which Ctrl key to crouch
//            if (this.keyboardManager.IsKeyDown(GameConstants.RightKeys[6]))
//                rightAltHeld = true;
            
//            if (this.keyboardManager.IsKeyUp(GameConstants.RightKeys[6]) && rightAltHeld && rightCtrlEnabled == false)
//            {
//                rightCtrlEnabled = true;
//                rightAltHeld = false;
//            }
//            else if (this.keyboardManager.IsKeyUp(GameConstants.RightKeys[6]) && rightAltHeld && rightCtrlEnabled)
//            {
//                rightCtrlEnabled = false;
//                rightAltHeld = false;
//            }

//            if (rightCtrlEnabled && rightAltHeld == false)
//            {
//                // CROUCHING FOR RIGHT HAND ONLY
//                if (this.keyboardManager.IsKeyDown(GameConstants.RightKeys[5]) && (isCrouching == false || parent.Transform3D.Translation.Y > 5))
//                {
//                    parent.Transform3D.TranslateBy(new Vector3(0, -1f, 0));

//                    if (parent.Transform3D.Translation.Y < 5f)
//                    {
//                        parent.Transform3D.TranslateBy(new Vector3(0, 5f, 0));
//                        isCrouching = true;
//                    }

//                }
//                else if (this.keyboardManager.IsKeyUp(GameConstants.RightKeys[5]) && isCrouching == true && parent.Transform3D.Translation.Y < 8)
//                {
//                    parent.Transform3D.TranslateBy(new Vector3(0, 1f, 0));

//                    if (parent.Transform3D.Translation.Y > 8f)
//                    {
//                        parent.Transform3D.TranslateBy(new Vector3(0, 8f, 0));
//                        isCrouching = false;
//                    }

//                }
                
//            }
//            else if (rightCtrlEnabled == false && rightAltHeld == false)
//            {
//                // CROUCHING FOR LEFT HAND ONLY
//                if (this.keyboardManager.IsKeyDown(GameConstants.LeftKeys[5]) && (isCrouching == false || parent.Transform3D.Translation.Y > 5))
//                {
//                    parent.Transform3D.TranslateBy(new Vector3(0, -1f, 0));

//                    if (parent.Transform3D.Translation.Y < 5f)
//                    {
//                        parent.Transform3D.TranslateBy(new Vector3(0, 5f, 0));
//                        isCrouching = true;
//                    }

//                }
//                else if (this.keyboardManager.IsKeyUp(GameConstants.LeftKeys[5]) && isCrouching == true && parent.Transform3D.Translation.Y < 8)
//                {
//                    parent.Transform3D.TranslateBy(new Vector3(0, 1f, 0));

//                    if (parent.Transform3D.Translation.Y > 8f)
//                    {
//                        parent.Transform3D.TranslateBy(new Vector3(0, 8f, 0));
//                        isCrouching = false;
//                    }

//                }
//            }
//            #endregion


//            // Constrain movement in Y-axis
//            moveVector.Y = 0;

//            // Move everything with respect to deltaTime
//            parent.Transform3D.TranslateBy(moveVector * gameTime.ElapsedGameTime.Milliseconds);
//        }

//        private void HandleMouseInput(GameTime gameTime, Actor3D parent)
//        {
//            this.mouseManager.MouseVisible = false;

//            // Set the mouse position to the center of window once at start
//            while (setLook == false)
//            {
//                this.mouseManager.SetPosition(new Vector2(512, 384));
//                setLook = true;
//            }

//            Vector2 mouseDelta = this.mouseManager.GetDeltaFromCentre(new Vector2(512, 384));
//            mouseDelta *= this.rotationSpeed * gameTime.ElapsedGameTime.Milliseconds;

//            if (mouseDelta.Length() != 0)
//            {              
//                if (this.mouseManager.HasMovedRight(0.01f))
//                {
//                    if (this.mouseManager.Position.X > 1022.9f)
//                    {
//                        float mousePosY = this.mouseManager.Position.Y;

//                        this.mouseManager.SetPosition(new Vector2(1, mousePosY));
//                    }
//                }
//                else if (this.mouseManager.HasMovedLeft(0.01f))
//                {
//                    if (this.mouseManager.Position.X < 1)
//                    {
//                        float mousePosY = this.mouseManager.Position.Y;

//                        this.mouseManager.SetPosition(new Vector2(1022.9f, mousePosY));
//                    }
//                }           
                
//                parent.Transform3D.RotateBy(new Vector3(-2.2f * mouseDelta.X, -1 * mouseDelta.Y, 0));                    
//            }
//        }

//        public object Clone()
//        {
//            throw new NotImplementedException();
//        }

//        public ControllerType GetControllerType()
//        {
//            throw new NotImplementedException();
//        }
//    }
//}
